class RenameTroopForeignKeys < ActiveRecord::Migration
  def change
  	rename_column :trooplinks, :troop_id, :deal_id
  	rename_column :troopcompletes, :troop_id, :deal_id
  	rename_column :troopfails, :troop_id, :deal_id
  	rename_column :orders, :troop_id, :deal_id
  	rename_column :referrals, :troop_id, :deal_id
  end
end
