class CreateTroops < ActiveRecord::Migration
  def change
    create_table :troops do |t|
      t.integer :user_id
      t.integer :product_id
      t.string :slug
      t.decimal :unit_price
      t.decimal :troop_limit
      t.integer :isenabled
      t.date :auction_start
      t.date :auction_end
      t.string :status

      t.timestamps
    end
  end
end



 
