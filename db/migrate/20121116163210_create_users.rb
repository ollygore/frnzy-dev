class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.text :password_digest
      t.string :first_name
      t.string :last_name
      t.string :username
      t.string :company
      t.string :profile_image
      t.string :location
      t.string :url_web
      t.string :url_twitter
      t.string :url_facebook
      t.integer :role_id
      t.integer :isenabled

      t.timestamps
    end
  end
end
