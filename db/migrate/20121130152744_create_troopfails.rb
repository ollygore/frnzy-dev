class CreateTroopfails < ActiveRecord::Migration
  def change
    create_table :troopfails do |t|
      t.integer :user_id
      t.integer :troop_id

      t.timestamps
    end
  end
end
