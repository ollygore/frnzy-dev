class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.text :content
      t.string :slug
      t.string :title
      t.text :metadescription
      t.string :metatitle
      t.string :metakeyword

      t.timestamps
    end
  end
end
