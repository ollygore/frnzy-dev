class Addorders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
    
      t.integer :troop_id
      t.integer :user_id
      t.float :amount
      t.string :paypal_email
      t.string :paypal_name
      t.integer :order_type
      t.string :transaction_paypal
      

      t.timestamps
    end
  end
end