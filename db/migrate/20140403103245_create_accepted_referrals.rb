class CreateAcceptedReferrals < ActiveRecord::Migration
  def change
    create_table :accepted_referrals do |t|
      t.references :referral
      t.references :user

      t.timestamps
    end
    add_index :accepted_referrals, :referral_id
    add_index :accepted_referrals, :user_id
  end
end
