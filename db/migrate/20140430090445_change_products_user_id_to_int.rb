class ChangeProductsUserIdToInt < ActiveRecord::Migration
  def up
  	execute 'ALTER TABLE products ALTER COLUMN user_id TYPE integer USING (user_id::integer)'
  end

  def down
  end
end
