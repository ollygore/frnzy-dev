class CreateSettings < ActiveRecord::Migration
  def change
  	drop_table :settings
    create_table :settings do |t|
      t.date :lastrun
      t.text :message

      t.timestamps
    end
  end
end
