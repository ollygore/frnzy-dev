class ChangeProductsUrlType < ActiveRecord::Migration
  def up
  	change_table :products do |t|
      t.change :product_url, :text
    end
  end

  def down
  end
end
