class Addaddresstouser < ActiveRecord::Migration
  def change
  add_column :users, :house_number, :string
  add_column :users, :address, :string
  add_column :users, :address_2, :string
  add_column :users, :postcode, :string
  add_column :users, :city, :string
  add_column :users, :country, :string 
  end
end
