class AddSiteSettingForResponder < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.date :lastrun
      t.text :message
      t.timestamps
    end
  end
end
