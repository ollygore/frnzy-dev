class Popularity < ActiveRecord::Migration
  def change  
    add_column :products, :popularity, :integer
    add_column :troops, :popularity, :integer
  end
end
