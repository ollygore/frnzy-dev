class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.string :image_url
      t.string :slug
      t.string :barcode
      t.decimal :price
      t.string :user_id
      t.integer :category_id
      t.integer :isenabled

      t.timestamps
    end
  end
end
