class CreateTrooplinks < ActiveRecord::Migration
  def change
    create_table :trooplinks do |t|
      t.integer :user_id
      t.integer :troop_id

      t.timestamps
    end
  end
end
