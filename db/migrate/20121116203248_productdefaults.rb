class Productdefaults < ActiveRecord::Migration
  def change
    change_column :products, :isenabled, :integer, :default => 0
  end
end
