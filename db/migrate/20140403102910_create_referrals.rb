class CreateReferrals < ActiveRecord::Migration
  def change
    create_table :referrals do |t|
      t.references :user_id
      t.string :email_addr
      t.string :referral_code
      t.references :product
      t.references :troop

      t.timestamps
    end
    add_index :referrals, :user_id_id
    add_index :referrals, :product_id
    add_index :referrals, :troop_id
  end
end
