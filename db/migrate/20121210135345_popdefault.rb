class Popdefault < ActiveRecord::Migration
  def change
  
  change_column :troops, :popularity, :integer, :default => 0
  change_column :products, :popularity, :integer, :default => 0
  
  end
end
