# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141201192548) do

  create_table "accepted_referrals", :force => true do |t|
    t.integer  "referral_id"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "accepted_referrals", ["referral_id"], :name => "index_accepted_referrals_on_referral_id"
  add_index "accepted_referrals", ["user_id"], :name => "index_accepted_referrals_on_user_id"

  create_table "carts", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "slug"
  end

  create_table "comments", :force => true do |t|
    t.string   "title",            :limit => 50, :default => ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], :name => "index_comments_on_commentable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "deals", :force => true do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.string   "slug"
    t.decimal  "price"
    t.decimal  "troop_limit"
    t.integer  "isenabled",   :default => 0
    t.string   "status"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.datetime "auction_end"
    t.integer  "location_id"
    t.integer  "end_count"
    t.integer  "popularity",  :default => 0
    t.boolean  "featured",    :default => false
    t.decimal  "troop_rrp"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "follows", :force => true do |t|
    t.integer  "user_id"
    t.integer  "follower_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "category_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "slug"
  end

  create_table "likes", :force => true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "locations", :force => true do |t|
    t.string   "region"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "slug"
    t.decimal  "gps_lat"
    t.decimal  "gps_long"
    t.decimal  "radius"
  end

  create_table "orders", :force => true do |t|
    t.integer  "deal_id"
    t.integer  "user_id"
    t.float    "amount"
    t.string   "paypal_email"
    t.string   "paypal_name"
    t.integer  "order_type"
    t.string   "transaction_paypal"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.float    "total"
    t.string   "transactionid"
  end

  create_table "pages", :force => true do |t|
    t.text     "content"
    t.string   "slug"
    t.string   "title"
    t.text     "metadescription"
    t.string   "metatitle"
    t.string   "metakeyword"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image_url"
    t.string   "slug"
    t.string   "barcode"
    t.decimal  "price"
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "isenabled",                  :default => 0
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "product_image_file_name"
    t.string   "product_image_content_type"
    t.integer  "product_image_file_size"
    t.datetime "product_image_updated_at"
    t.datetime "auction_end"
    t.integer  "popularity",                 :default => 0
    t.integer  "group_id"
    t.boolean  "featured",                   :default => false
    t.text     "product_url"
  end

  create_table "referrals", :force => true do |t|
    t.integer  "user_id"
    t.string   "email_addr"
    t.string   "referral_code"
    t.integer  "product_id"
    t.integer  "deal_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "referrals", ["deal_id"], :name => "index_referrals_on_troop_id"
  add_index "referrals", ["product_id"], :name => "index_referrals_on_product_id"
  add_index "referrals", ["user_id"], :name => "index_referrals_on_user_id_id"

  create_table "roles", :force => true do |t|
    t.string   "role_type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "settings", :force => true do |t|
    t.date     "lastrun"
    t.text     "message"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "shopping_cart_items", :force => true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "quantity"
    t.integer  "item_id"
    t.string   "item_type"
    t.float    "price"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "troopcompletes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "deal_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "transaction_status"
    t.integer  "order_id"
  end

  create_table "troopfails", :force => true do |t|
    t.integer  "user_id"
    t.integer  "deal_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "order_id"
    t.integer  "refund_id"
  end

  create_table "trooplinks", :force => true do |t|
    t.integer  "user_id"
    t.integer  "deal_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "order_id"
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.text     "password_digest"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "company"
    t.string   "profile_image"
    t.string   "location"
    t.string   "url_web"
    t.string   "url_twitter"
    t.string   "url_facebook"
    t.integer  "role_id",                             :default => 3
    t.integer  "isenabled",                           :default => 0
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.integer  "location_id"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "house_number"
    t.string   "address"
    t.string   "address_2"
    t.string   "postcode"
    t.string   "city"
    t.string   "country"
    t.string   "provider"
    t.string   "oauth_token"
    t.time     "oauth_expires"
    t.integer  "uid",                    :limit => 8
    t.string   "email_confirm_token"
    t.datetime "email_confirm_sent_at"
    t.boolean  "email_confirmed",                     :default => false
    t.string   "auth_token"
  end

end
