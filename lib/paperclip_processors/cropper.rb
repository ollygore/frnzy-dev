module Paperclip
   class Cropper < Thumbnail
      def transformation_command
         if @target_geometry.to_s.include? '#'
            width = @target_geometry.to_s.split('x')[0]
            height = @target_geometry.to_s.split('x')[1].sub('#','')
            " -resize '#{width}x#{height}>' -gravity center -background white -extent '#{width}x#{height}!'"
         else
            super
         end
      end
   end
end