task :checkexpiry => :environment do

	include ActiveMerchant::Billing
  	include PaypalExpressHelper

     @deals_checker = Deal.where("isenabled = ? AND auction_end <= ? AND status = ?", '1', DateTime.now.utc, '1')
     	@deals_checker.each do | deal |
		
			if (deal.trooplinks.count >= deal.troop_limit)
				deal.status = '2';
				deal.end_count = deal.trooplinks.count	
				
				deal.trooplinks.each do |trooplink|	
					Troopcomplete.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :transaction_status => 0, :order_id => trooplink.order_id)
					#congratulations email				
					@user = User.find(trooplink.user_id)
					@user.send_troop_ended(trooplink)
					trooplink.destroy
				end
				
			else
			
				deal.status = '3';
			
				deal.end_count = deal.trooplinks.count
		
				if deal.end_count > 0 
			
					deal.trooplinks.each do |trooplink|	
	
						@gateway ||= PaypalExpressGateway.new(
     						:login => PaypalLogin.login,
							:password => PaypalLogin.password,
							:signature => PaypalLogin.signature,
     					)
					
						#refund = @gateway.transfer(trooplink.order.total * 100, trooplink.order.paypal_email, :currency => 'GBP', :subject => "FRNZY refund", :note => trooplink.deal.product.name)
                  		refund = @gateway.refund nil, trooplink.order.transactionid

		                  if refund.success?

		                     @order = Order.new
		                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => trooplink.order.paypal_email, :paypal_name => trooplink.order.paypal_name, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
		                     @order.save!

		                  else

		                     @order = Order.new
		                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => refund.message, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
		                     @order.save!

		                  end
				
						Troopfail.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :order_id => @order.id)
						Like.create(:user_id => trooplink.user_id, :product_id => deal.product_id)
						#failure email
						@user = User.find(trooplink.user_id)
						@user.send_troop_failed(trooplink)
						trooplink.destroy
					end
				end
			end
			deal.save	
		end

end  
