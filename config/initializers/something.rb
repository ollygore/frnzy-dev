Paperclip.interpolates(:s3_eu_url) { |attachment, style|
  "#{attachment.s3_protocol}://s3-eu-west-1.amazonaws.com/#{attachment.bucket_name}/#{attachment.path(style).gsub(%r{^/}, "")}"
}

# config/initializers/paperclip.rb
Paperclip::Attachment.default_options.merge!(
  s3_protocol:          'https'
)