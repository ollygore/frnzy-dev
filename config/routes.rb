St001::Application.routes.draw do

   resources :pages

   get "/how_it_works" => "pages#how", :as => "how_it_works"
   get "/win_urBeats_by_dre" => "pages#beats", :as => "win_urBeats_by_dre"

   get "email_confirms/new"

   match '/sharrre' => 'Rsocialize#sharrre'

   match '/deals/new/:id' => 'deals#new', :as => 'newdealwithproduct'


   get "invite/products/:id/" => "products#invite", :as => "invite_product"
   get "invite/deal/:id/" => "deals#invite", :as => "invite_deal"
   get "invite/home/" => "home#invite", :as => "invite_home"
   post "invite/email/" => "referral#email", :as => "invite_email"
   match 'invite/:id' => 'referral#redirect', :as => 'accept_referral'

   resources :groups
   resources :troopfails
   resources :troopcompletes
   resources :locations
   resources :categories
   resources :trooplinks
   resources :deals
   resources :orders
   resources :likes, :only => [:create, :destroy]
   resources :password_resets

   resources :products do
      post :want_product, :on => :member
   end

   get "home/index"
   resources :roles
   resources :likes
   resource :cart

   get "paypal_express/checkout"
   get "paypal_express/cancel"
   get "paypal_express/review"
   get "paypal_express/purchase"
   get "paypal_express/refund"
   get "paypal_express/notify"
   post "paypal_express/notify"
   
   get "/check_duplicates" => "products#duplicate", :as => "product_duplicate"
   get "/like_and_view/:id/" => "products#like_and_view", :as =>"producut_like_and_view"
   get "/product_url_scrape" => "scrape#scrape", :as => "scrape_url_images"

   match "/products/add_new_comment" => "products#add_new_comment", :as => "add_new_comment_to_produdcts", :via => [:post]
   match "/deals/add_new_comment" => "deals#add_new_comment", :as => "add_new_comment_to_deals", :via => [:post]
  
   get "remove_comment/:id/" => "products#remove_comment", :as => "remove_comment"
   
   get "shareproduct/:id/" => "products#share", :as => "share_product"
   get "sharedeal/:id/" => "deals#share", :as => "share_deal"

   get "followuser/:id/" => "follows#followuser", :as => "followuser"
   get "unfollowuser/:id/" => "follows#unfollowuser", :as => "unfollowuser"
   get "following/:id/" => "follows#following", :as => "following"
   get "followers/:id/" => "follows#followers", :as => "followers"

   match '/search/:query/:qcategory' => 'search#search', :as => 'search'
   #match '/search/:query' => 'search#search', :as => 'search'
   
   match '/search' => 'search#search', :as => 'search'

   match 'auth/:provider/callback', to: 'sessions#createfacebook'
   match 'auth/failure', to: redirect('/')
   match 'signout', to: 'sessions#facebook', as: 'signout'


   get "logout" => "sessions#destroy", :as => "logout"
   get "login" => "sessions#new", :as => "login"
   get "admin_users" => "users#view_user", :as => "admin_users"
   get "admin_merchants" => "users#view_merchant", :as => "admin_merchants"
   get "admin_products" => "products#view", :as => "admin_products"
   get "admin_deals" => "deals#view", :as => "admin_deals"
   get "admin_locations" => "locations#view", :as => "admin_locations"
   get "admin_categories" => "categories#view", :as => "admin_categories"
   get "admin_orders" => "orders#view", :as => "admin_orders"
   get "admin_referrals" => "referral#view", :as => "admin_referrals"


   get "admin_pages" => "pages#view", :as => "admin_pages"
   get "register" => "users#new", :as => "register"
   get "dashboard" => "users#dashboard", :as => "userprofile"

   get "sitemap.xml.gz" => "sitemap#file"

   match "/modalshow/:id" => "deals#modalshow"

   root :to => "products#homepage"
   resources :users
   resources :sessions

   get "email_confirms/new"
   resources :email_confirms


   match "/resend/:id" => "users#resend_confirmation_email"

   match 'contactmessager' => 'contactmessager#new', :as => 'contactmessager', :via => :get
   match 'contactmessager' => 'contactmessager#create', :as => 'contactmessager', :via => :post
   
   match 'troops/:id' => 'deals#show', :as => 'troop_catch'

end

