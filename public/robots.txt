# See http://www.robotstxt.org/wc/norobots.html for documentation on how to use the robots.txt file
#
# To ban all spiders from the entire site uncomment the next two lines:
# User-Agent: *
# Disallow: /

sitemap: http://frnzy.s3.amazonaws.com/sitemaps/sitemap.xml.gz
sitemap: http://www.frnzy.co.uk/sitemap.xml.gz