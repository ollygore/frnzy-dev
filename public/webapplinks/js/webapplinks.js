//Main Responsive Menu Script
$(function() {
	var items = $('.overlapblackbg, .slideLeft');
	var webapplinkscontent = $('.webapplinkscontent');
	
	var open = function() {
	$(items).removeClass('close').addClass('open');
						}
	var close = function() { 
	$(items).removeClass('open').addClass('close');
	}

	$('#navToggle').click(function(){
		if (webapplinkscontent.hasClass('open')) {$(close)}
		else {$(open)}
	});
	webapplinkscontent.click(function(){
		if (webapplinkscontent.hasClass('open')) {$(close)}
	});
	
	$('#navToggle,.overlapblackbg').on('click', function(){
	$('.webapplinkscontainer').toggleClass( "mrginleft" );
	});

	$('.webapplinks-list li').has('ul').prepend('<span class="webapplinks-click dropeft"><i class="webapplinks-arrow fa fa-sort-down"></i></span>');

	$('.webapplinks-list li').has('.halfdiv').prepend('<span class="webapplinks-click dropeft"><i class="webapplinks-arrow fa fa-sort-down"></i></span>');
		
	// Click to expand the webapplinks
	$('.webapplinks-mobile').click(function(){
		$('.webapplinks-list').slideToggle('slow');
	});
	$('.webapplinks-click').click(function(){
	$(this).siblings('.webapplinks-submenu').slideToggle('slow');
	$(this).siblings('.webapplinks-submenu-sub').slideToggle('slow');
	$(this).siblings('.webapplinks-submenu-sub-sub').slideToggle('slow');
	$(this).siblings('.megamenu').slideToggle('slow');
	});

});
//Main Responsive Menu Script


//On Click Drop Water Script

$(function () {
    var water, ady, a, b;
    $('.dropeft').click(function (e) {
        if ($(this).find('.water').length === 0) {
            $(this).prepend('<em class=\'water\'></em>');
        }
        water = $(this).find('.water');
        water.removeClass('drop-animate');
        if (!water.height() && !water.width()) {
            ady = Math.max($(this).outerWidth(), $(this).outerHeight());
            water.css({
                height: ady,
                width: ady
            });
        }
        b = e.pageY - $(this).offset().top - water.height() / 2;
		a = e.pageX - $(this).offset().left - water.width() / 2;
        water.css({
            top: b + 'px',
            left: a + 'px'
        }).addClass('drop-animate');
    });
});
//On Click Drop Water Script








