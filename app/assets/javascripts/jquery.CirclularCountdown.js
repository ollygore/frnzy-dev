/*
Item Name : Circular Countdown jQuery Plugin
Item URI : http://codecanyon.net/item/circular-countdown-jquery-plugin/3761921
Author URI : http://codecanyon.net/user/Pixelworkshop/
Version : 1.1
*/

(function ($) {

    var settings = {
      strokeEndsBackgroundColor:'rgba(0,0,0,0.06)',
      strokeEndsColor:'rgba(255,199,0,1)',
      strokeDaysBackgroundColor:'rgba(0,0,0,0.06)',
      strokeDaysColor:'rgba(3,164,0,1)',
      strokeHoursBackgroundColor:'rgba(0,0,0,0.06)',
      strokeHoursColor:'rgba(255,31,136,1)',
      strokeMinutesBackgroundColor:'rgba(0,0,0,0.06)',
      strokeMinutesColor:'rgba(0,179,247,1)',
      strokeSecondsBackgroundColor:'rgba(0,0,0,0.06)',
      strokeSecondsColor:'rgba(244,0,0,1)',
      // Stroke width can be set to a number equal to 10% of the total width 
      // of the countdown to get full circles instead of strokes.
      // If your countdown has a width of 800px, set the strokeWidth to 80
      // to get full circles. Use 70 if your countdown has a width of 700px, etc.

      strokeWidth:6,
      strokeBackgroundWidth:6,
      countdownEasing:'easeOutBounce',
      countdownTickSpeed:400,
      backgroundShadowColor: 'rgba(0,0,0,0.2)',
      backgroundShadowBlur: 0,
      strokeShadowColor: 'rgba(0,0,0,0)',
      strokeShadowBlur: 0
    };


    var methods = {
        
        init:function (options) {
            
            settings = $.extend(1, settings, options);

            return this.each(function () {

               countdownTimer(options.until, this);
               countdownArcs(this);

            }); // End each

        },

        update:function (options) {
            settings = $.extend(1, settings, options);
        }
    };


    $.fn.circularCountdown = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('No found method ' + method);
        }

    };


   function countdownArcs(target) { 
        var circularCountdown = $('.circular_countdown_days',target),
         countdownElementRadius = ($(target).find('.countdown_timer li').width() - settings.strokeWidth) / 2,
         countdownElementWidth = countdownElementRadius * 2 + settings.strokeWidth,
         countdownXPosition = countdownElementWidth / 2,
         countdownYPosition = countdownElementWidth / 2;


         $(".circular_countdown_ends",target)
            .drawArc({
               layer: true,
               name: "ends_bg",
               strokeStyle: settings.strokeEndsBackgroundColor,
               strokeWidth: settings.strokeBackgroundWidth,
               radius: countdownElementRadius,
               shadowColor: settings.backgroundShadowColor,
               shadowBlur: settings.backgroundShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
            .drawArc({
               layer: true,
               name: "ends",
               strokeStyle: settings.strokeEndsColor,
               strokeWidth: settings.strokeWidth,
               radius: countdownElementRadius,
               shadowColor: settings.strokeShadowColor,
               shadowBlur: settings.strokeShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
         $(".circular_countdown_days",target)
            .drawArc({
               layer: true,
               name: "days_bg",
               strokeStyle: settings.strokeDaysBackgroundColor,
               strokeWidth: settings.strokeBackgroundWidth,
               radius: countdownElementRadius,
               shadowColor: settings.backgroundShadowColor,
               shadowBlur: settings.backgroundShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
            .drawArc({
               layer: true,
               name: "days",
               strokeStyle: settings.strokeDaysColor,
               strokeWidth: settings.strokeWidth,
               radius: countdownElementRadius,
               shadowColor: settings.strokeShadowColor,
               shadowBlur: settings.strokeShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
         $(".circular_countdown_hours",target)
            .drawArc({
               layer: true,
               name: "hours_bg",
               strokeStyle: settings.strokeHoursBackgroundColor,
               strokeWidth: settings.strokeBackgroundWidth,
               radius: countdownElementRadius,
               shadowColor: settings.backgroundShadowColor,
               shadowBlur: settings.backgroundShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
            .drawArc({
               layer: true,
               name: "hours",
               strokeStyle: settings.strokeHoursColor,
               strokeWidth: settings.strokeWidth,
               radius: countdownElementRadius,
               shadowColor: settings.strokeShadowColor,
               shadowBlur: settings.strokeShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
         $(".circular_countdown_minutes",target)
            .drawArc({
               layer: true,
               name: "minutes_bg",
               strokeStyle: settings.strokeMinutesBackgroundColor,
               strokeWidth: settings.strokeBackgroundWidth,
               radius: countdownElementRadius,
               shadowColor: settings.backgroundShadowColor,
               shadowBlur: settings.backgroundShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
            .drawArc({
               layer: true,
               name: "minutes",
               strokeStyle: settings.strokeMinutesColor,
               strokeWidth: settings.strokeWidth,
               radius: countdownElementRadius,
               shadowColor: settings.strokeShadowColor,
               shadowBlur: settings.strokeShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
         $(".circular_countdown_seconds",target)
            .drawArc({
               layer: true,
               name: "seconds_bg",
               strokeStyle: settings.strokeSecondsBackgroundColor,
               strokeWidth: settings.strokeBackgroundWidth,
               radius: countdownElementRadius,
               shadowColor: settings.backgroundShadowColor,
               shadowBlur: settings.backgroundShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })
            .drawArc({
               layer: true,
               name: "seconds",
               strokeStyle: settings.strokeSecondsColor,
               strokeWidth: settings.strokeWidth,
               radius: countdownElementRadius,
               shadowColor: settings.strokeShadowColor,
               shadowBlur: settings.strokeShadowBlur,
               x: countdownXPosition, 
               y: countdownYPosition
            })

            .drawLayers()

   }


   function countdownAnimation(target) { 
      
      var $targParent = $(target).parent();

      var tempTxt = $targParent.find('.countdown_timer ul li.days em').text();
      $targParent.find(".circular_countdown_days")
         .animateLayer("days", {
            end: tempTxt * (360/14) //0.9863 - JS Changed value to make circle full at 14days.
         }, settings.countdownTickSpeed, settings.countdownEasing);

      tempTxt = $targParent.find('.countdown_timer ul li.hours em').text();
      $targParent.find(".circular_countdown_hours")
         .animateLayer("hours", {
            end: tempTxt * 15
         }, tempTxt == '23' ? 0 : settings.countdownTickSpeed, settings.countdownEasing);

      tempTxt = $targParent.find('.countdown_timer ul li.minutes em').text();
      $targParent.find(".circular_countdown_minutes")
         .animateLayer("minutes", {
            end: tempTxt * 6
         }, tempTxt == '59' ? 0 : settings.countdownTickSpeed, settings.countdownEasing);
      
      tempTxt = $targParent.find('.countdown_timer ul li.seconds em').text();
      $targParent.find(".circular_countdown_seconds")
         .animateLayer("seconds", {
            end:tempTxt * 6
         }, tempTxt == '59' ? 0 : settings.countdownTickSpeed, settings.countdownEasing);
               
   }


   function countdownTimer(untilDateTime, target) { 

      $.countdown.regional['en'] = {
         labels:['years','months','weeks','days','hours','mins','secs'],
         labels1:['year','month','week','day','hour','min','sec'],
         compactLabels:['y','m','w','d'],
         whichLabels:null,
         digits:['0','1','2','3','4','5','6','7','8','9'],
         timeSeparator:':',
         isRTL:false
      }
      $.countdown.setDefaults($.countdown.regional['en']);
      $(target).find('.countdown_timer').countdown({

         // new Date(year, mth, day, hr, min, sec) - Date / Time to count down to 

         // How to format the date :
         // Year,
         // Month (january = 0, february = 1; etc.),
         // Day of the month (2 for the 2nd, 3 for the 3rd, etc.),
         // Hour of the day (using the 24-hour system) 
         // Optionnally you can add minutes and seconds separated by commas

         // Examples : 
         // new Date(2014, 2, 15, 18) will count until the 15th of March 2014 at 6 PM.
         // new Date(2013, 6, 3, 8) will count until the 3rd of July 2013 at 8 AM.

         until: untilDateTime, 
         timezone: 1,
         format: 'DHMS',
         layout: 
            '<ul>' +
            '<li class="ends"><em>DEAL</em>ends in</li>' +
            '{d<}<li class="days"><em>{dn}</em> {dl}</li>{d>}' +
            '{h<}<li class="hours"><em>{hn}</em> {hl}</li>{h>}' +
            '{m<}<li class="minutes"><em>{mn}</em> {ml}</li>{m>}' +
            '{s<}<li class="seconds"><em>{sn}</em> {sl}</li>{s>}' +
            '</ul>',

         onTick: function() {
            countdownAnimation(this);
         }

      });
               
   }


})(jQuery);