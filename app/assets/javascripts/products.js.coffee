# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  if $('#endlesspagination .pagination').length
    $(window).scroll ->
      url = $('.pagination .next_page').attr('href')
      if url && $(window).scrollTop() > $(document).height() - $(window).height() - 50
        $('.pagination').text("Loading more products and deals...")
        $.getScript(url)
        
    $(window).scroll()
    

  $('.field #product_group_id').parent().hide()
  states = $('.field #product_group_id').html()
  $('.field #product_category_id').change ->
    country = $('.field #product_category_id :selected').text()
    escaped_country = country.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(states).filter("optgroup[label='#{escaped_country}']").html()
    if options
      $('.field #product_group_id').html(options)
      $('.field #product_group_id').parent().show()
    else
      $('.field #product_group_id').empty()
      $('.field #product_group_id').parent().hide()
      
      
