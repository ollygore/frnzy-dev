// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require fancybox
//= require jquery_ujs
//= require twitter/bootstrap
//= require isotope.pkgd.min.js
//= require jquery.jCanvas
//= require jquery.Easing
//= require jquery.countdown
//= require jquery.CirclularCountdown
//= require jquery-ui.min
//= require jquery-ui-timepicker-addon
//= require dataTables/jquery.dataTables
//= require flowtype
//= require general
//= require_tree .


jQuery(window).on("load resize",function(e){
	var $container = $('.isotope'),
	colWidth = function () {
		var w = $container.width(), 
		columnNum = 1,
		columnWidth = 0;
	//Select what will be your porjects columns according to container widht
	if (w > 2000)     { columnNum  = 10; }  
	else if (w > 1840) { columnNum  = 9; }  
	else if (w > 1640) { columnNum  = 8; }  
	else if (w > 1440) { columnNum  = 7; }  
	else if (w > 1240) { columnNum  = 6; }  
	else if (w > 1040) { columnNum  = 5; }  
	else if (w > 850) { columnNum  = 4; }  
	else if (w > 768) { columnNum  = 3; }  
	else if (w > 480) { columnNum  = 3; }
	else if (w > 300) { columnNum  = 2; }
	columnWidth = Math.floor(w/columnNum);

	//Default item width and height
	$container.find('.frnzy_item').each(function() {
		var $item = $(this), 
		width = columnWidth,
		height = columnWidth;
		$item.css({ width: width, height: height });
	}); 

	//2x width item width and height
	$container.find('.width2').each(function() {
		var $item = $(this), 
		width = columnWidth*2,
		height = columnWidth;
		$item.css({ width: width, height: height });
	}); 

	//2x height item width and height
	$container.find('.height2').each(function() {
		var $item = $(this), 
		width = columnWidth,
		height = columnWidth*2;
		$item.css({ width: width, height: height });
	}); 

	//2x item width and height
	$container.find('.width2.height2').each(function() {
		var $item = $(this), 
		width = columnWidth*2,
		height = columnWidth*2;
		$item.css({ width: width, height: height });
	}); 
	return columnWidth;
	},
	isotope = function () {
		$container.isotope({
			hiddenStyle: {
        opacity: 0
      },
      visibleStyle: {
        opacity: 1
      },
			resizable: true,
			itemSelector: '.frnzy_item',
			masonry: {
				columnWidth: colWidth(),
				gutterWidth: 10,
				gutter: 10
			}
		});
	};
	isotope(); 

	// bind filter button click
	$('.isotope-filters').on( 'click', 'button', function() {
		var filterValue = $( this ).attr('data-filter');
		$container.isotope({ filter: filterValue });
	});

	// change active class on buttons
	$('.isotope-filters').each( function( i, buttonGroup ) {
		var $buttonGroup = $( buttonGroup );
		$buttonGroup.on( 'click', 'button', function() {
			$buttonGroup.find('.active').removeClass('active');
			$( this ).addClass('active');
		});
	}); 
	

	$( "#fade_on_load" ).delay(500).animate({
    	opacity: 1
	  }, 1000, function() {
	    // Animation complete.
	  });



});





