class ReferralController < ApplicationController

   layout 'application'
	#Determine URL to redirect too from Referral...
	def redirect
		#Store current referral ID in session...

		@referral = Referral.where(:referral_code => params[:id]).first
		if(@referral != nil)
			#Lookup referral ID
			session[:referral_id] = @referral.id
			cookies[:referral_id] = @referral.id
			if(@referral.product != nil)
				redirect_to "/products/#{@referral.product.slug}"
			elsif @referral.deal != nil
				redirect_to "/deals/#{@referral.deal.slug}"
			else
				redirect_to root_url
			end
		else
			redirect_to root_url
		end
		
	end

	def email
		@referral = Referral.where(:id => params[:referral_id]).first

		if(@referral.product != nil)
			begin
				UserMailer.invite_friend_to_product(@referral.user, params[:email], @referral.product, @referral).deliver	
				if 	params[:email2] != nil and params[:email2] != ""
					UserMailer.invite_friend_to_product(@referral.user, params[:email2], @referral.product, @referral).deliver	
				end
				if 	params[:email3] != nil and params[:email3] != ""
					UserMailer.invite_friend_to_product(@referral.user, params[:email3], @referral.product, @referral).deliver	
				end
				if 	params[:email4] != nil and params[:email4] != ""
					UserMailer.invite_friend_to_product(@referral.user, params[:email4], @referral.product, @referral).deliver	
				end
			rescue
			end
		elsif (@referral.deal)
			begin
				UserMailer.invite_friend_to_deal(@referral.user, params[:email], @referral.deal, @referral).deliver
				if 	params[:email2] != nil and params[:email2] != ""
					UserMailer.invite_friend_to_deal(@referral.user, params[:email2], @referral.deal, @referral).deliver
				end
				if 	params[:email3] != nil and params[:email3] != ""
					UserMailer.invite_friend_to_deal(@referral.user, params[:email3], @referral.deal, @referral).deliver
				end
				if 	params[:email4] != nil and params[:email4] != ""
					UserMailer.invite_friend_to_deal(@referral.user, params[:email4], @referral.deal, @referral).deliver
				end
			rescue
			end
		else
			begin
				UserMailer.invite_friend_to_home(@referral.user, params[:email], @referral).deliver
				if 	params[:email2] != nil and params[:email2] != ""
					UserMailer.invite_friend_to_home(@referral.user, params[:email2], @referral).deliver
				end
				if 	params[:email3] != nil and params[:email3] != ""
					UserMailer.invite_friend_to_home(@referral.user, params[:email3], @referral).deliver
				end
				if 	params[:email4] != nil and params[:email4] != ""
					UserMailer.invite_friend_to_home(@referral.user, params[:email4], @referral).deliver
				end
			rescue
			end
		end

		respond_to do |format|
            format.js
         end
	end

	def view
		@categories = Category.all
		@groups = Group.all

		if params[:product] != nil
			@referrals = Referral.select("referrals.*, COUNT (accepted_referrals.id) as accepted_count")
								 .joins("LEFT OUTER JOIN accepted_referrals ON accepted_referrals.referral_id = referrals.id")
								 .group("referrals.id")
								 .where(product_id: params[:product])
								 .order("accepted_count DESC")
		elsif params[:deal] != nil
			@referrals = Referral.select("referrals.*, COUNT (accepted_referrals.id) as accepted_count")
								 .joins("LEFT OUTER JOIN accepted_referrals ON accepted_referrals.referral_id = referrals.id")
								 .group("referrals.id")
								 .where(deal_id: params[:deal])
								 .order("accepted_count DESC")
		elsif params[:user] != nil
			@referrals = Referral.select("referrals.*, COUNT (accepted_referrals.id) as accepted_count")
								 .joins("LEFT OUTER JOIN accepted_referrals ON accepted_referrals.referral_id = referrals.id")
								 .group("referrals.id")
								 .where(user_id: params[:user])
								 .order("accepted_count DESC")
		else 
			@referrals = Referral.select("referrals.*, COUNT (accepted_referrals.id) as accepted_count")
								 .joins("LEFT OUTER JOIN accepted_referrals ON accepted_referrals.referral_id = referrals.id")
								 .group("referrals.id")
								 .order("accepted_count DESC")
		end
		respond_to do |format|
			format.html
		end
	end
end
