class SitemapController < ApplicationController

	def index
		#TODO: display here a HTML Sitemap
	end

	def file
		#stream the file here from S3 out to the user for download.
		file_name = "sitemap.xml.gz"
		data = open("https://s3-eu-west-1.amazonaws.com/frnzy/sitemaps/#{file_name}") 
		send_data data.read, :filename => file_name, :type => "application/gzip", :disposition => 'attachment', :stream => 'true', :buffer_size => '4096'
	end
end
