class HomeController < ApplicationController
   
   def index

   end

   def invite
      @user = current_user

      if Referral.uniq.exists?(:user_id => @user.id, :product_id => nil, :deal_id => nil)  
         @referral = Referral.where("user_id = ? AND product_id IS NULL AND deal_id IS NULL",@user.id).first
      else
         @referral = Referral.new
         @referral.referral_code = rand(36**8).to_s(36)
         @referral.user = @user
         @referral.save
      end

      respond_to do |format|
         format.js
      end
   end
end
