class SearchController < ApplicationController




   def search


      if cookies[:wanted_filter].present?
      else
         cookies[:wanted_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:active_filter].present?
      else
         cookies[:active_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:done_filter].present?
      else
         cookies[:done_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:sort_order].present?
      else
         cookies[:sort_order] = {:value => 'Most recent',:expires => 1.year.from_now,:path => '/'}
      end




      # Use params[:query] to perform the search
      # Use params[:qcategory] to perform the category filter
      @deals = Deal.where("isenabled = '1'")
      @products = Product.where("isenabled = '1'")


      if params[:qcategory].nil?
         @products = Product.search(params[:query])
      else
         @products = Product.searchwithcat(params[:query],params[:qcategory])
      end

      @searchquery = params[:query];

      if params[:qcategory].nil?
         @deals = Deal.search(params[:query])
      else
         @deals = Deal.searchwithcat(params[:query],params[:qcategory])
      end


      @users = User.all
      @likes = Like.all


      @deals = @deals.where(:status => ['1','2'])
      if (cookies[:active_filter] == '0') && (cookies[:done_filter] == '0')
         @deals = @deals.where(:status => ['4'])
      elsif (cookies[:active_filter] == '1') && (cookies[:done_filter] == '0')
         @deals = @deals.where(:status => ['1'])
      elsif (cookies[:active_filter] == '0') && (cookies[:done_filter] == '1')
         @deals = @deals.where(:status => ['2'])
      elsif (cookies[:active_filter] == '1') && (cookies[:done_filter] == '1')
         @deals = @deals.where(:status => ['1','2'])
      end

      @deallinks = Trooplink.all
      @locations = Location.all
      @categories = Category.all


      if cookies[:wanted_filter] == '1'
         @combinedproductsandtroops = (@products + @deals)
      else
         @combinedproductsandtroops = (@deals)
      end



      #SORT BY ELSIFS
      if (cookies[:sort_order] == 'Most recent')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:created_at]}.reverse
      elsif (cookies[:sort_order] == 'Oldest')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:created_at]}
      elsif (cookies[:sort_order] == 'Troops ending soon')
         @combinedproductsandtroops = @deals.where(:status => ['1']).sort_by{|e| e[:auction_end]}
      elsif (cookies[:sort_order] == 'Most expensive')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:price]}.reverse
      elsif (cookies[:sort_order] == 'Best deals')
         @combinedproductsandtroops = @deals.where(:status => ['1'])#@combinedproductsandtroops.sort_by{|e| e[:price]}
      elsif (cookies[:sort_order] == 'Most popular')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:popularity]}.reverse
      end


      #ORDER BY ON THIS ARRAY
      @combinedproductsandtroops = @combinedproductsandtroops.paginate(:page => params[:page], :per_page => 15)




      respond_to do |format|
         format.html # index.html.erb
         format.js
         format.json { render json: @combinedproductsandtroops }
      end

   end





end
