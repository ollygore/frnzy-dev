class PaypalExpressController < ApplicationController


   layout 'application'


   before_filter :extract_cart
   before_filter :assigns_gateway


   include ActiveMerchant::Billing
   include ActiveMerchant::Billing::Integrations
  
   include PaypalExpressHelper



   def checkout
      total_as_cents, setup_purchase_params = get_setup_purchase_params @cart, request
      setup_response = @gateway.setup_purchase(total_as_cents, setup_purchase_params)
      redirect_to @gateway.redirect_url_for(setup_response.token)
   end



   def cancel
      redirect_to '/cart'
   end


   def notify
      notify = Paypal::Notification.new(request.raw_post)
      if notify.acknowledge
         ordertoupdate = Order.where(:transaction_paypal => params[:item_number1]).first
         ordertoupdate.transactionid = notify.transaction_id
         ordertoupdate.save
      end
      render :nothing => true
   end


   def refund
      refund = @gateway.transfer(1, 'gibson@goreblimey.com', :currency => 'GBP', :subject => "ST Failed troop refund", :note => "Sorry")
      if refund.success?
         notice = "Refund successful"
      else
         notice = "Woops. Something went wrong while we were trying to complete the purchase with Paypal.
         Btw, here's what Paypal said: #{refund.message}"
      end
      redirect_to root_url, :notice => notice
   end




   def review
      if params[:token].nil?
         redirect_to root_url, :notice => 'Woops! Something went wrong!'
         return
      end

      gateway_response = @gateway.details_for(params[:token])

      unless gateway_response.success?
         redirect_to root_url,
         :notice => "Sorry! Something went wrong with the Paypal purchase. Here's what Paypal said: #{gateway_response.message}"
         return
      end

      @order_info = get_order_info gateway_response, @cart

      puts @order_info

   end





   def purchase

      if params[:token].nil? or params[:payer_id].nil?
         redirect_to root_url, :notice => "Sorry! Something went wrong with the Paypal purchase. Please try again later."
         return
      end

      total_as_cents, purchase_params = get_purchase_params @cart, request, params
      purchase = @gateway.purchase total_as_cents, purchase_params

      puts purchase
      puts 'bobdylan'

      if purchase.success?
         @cart.shopping_cart_items.collect do |line_item|

            deal = line_item.item

            @order = Order.new
            
            @order.attributes = {:transaction_paypal => session[:orderlink], :paypal_email => params[:paypal_email], :paypal_name => params[:paypal_name], :user_id => current_user.id, :total => params[:total], :order_type => 0, :deal_id => deal.id }
            @order.save!

            puts "PayPal Purchase"
            if session[:referral_id] != nil
               puts "with referal id: #{session[:referral_id].inspect}"
               ref = Referral.find_by_id(session[:referral_id])
               if ref.deal == deal
                  @accRef = AcceptedReferral.new
                  @accRef.referral = ref
                  @accRef.user = current_user
                  @accRef.save
               end
            elsif cookies[:referral_id] != nil
               puts "with cookie referal id: #{cookies[:referral_id].inspect}"
               ref = Referral.find_by_id(cookies[:referral_id])
               if ref.deal == deal
                  @accRef = AcceptedReferral.new
                  @accRef.referral = ref
                  @accRef.user = current_user
                  @accRef.save
               end
            end

            @user = current_user
            @user.send_troop_joined(@order,deal)

            @deallink = Trooplink.create(:user_id => current_user.id, :deal_id => deal.id, :order_id => @order.id)
            deal.popularity = deal.trooplinks.count
            deal.save
            

            #@product = Product.find_by_id(deal.product_id)
            begin
               @likes = Like.where(:product_id => deal.product_id, :user_id => current_user.id)
               @likes.each do |like|
                  like.destroy
               end
            rescue ActiveRecord::RecordNotFound
               puts 'No Likes found active record not found.'
            end
            session[:orderlink] = nil

         end

         @cart.clear

         notice = "Thank you! Once the deal ends we will confirm your purchase."

      else

         notice = "Woops. Something went wrong while we were trying to complete the purchase with Paypal.
         Btw, here's what Paypal said: #{purchase.message}"

      end

      redirect_to root_path, :notice => notice

   end






   private

   def extract_cart
      cart_id = session[:cart_id]
      @cart = session[:cart_id] ? Cart.find(cart_id) : Cart.create
      session[:cart_id] = @cart.id
   end


   def assigns_gateway
      @gateway ||= PaypalExpressGateway.new(
      :login => PaypalLogin.login,
      :password => PaypalLogin.password,
      :signature => PaypalLogin.signature,
      )
   end
end


