class ContactmessagerController < ApplicationController

  def new
    redirect_to '/'
  end
  

  def create
    @contactmessage = Contactmessage.new(params[:contactmessage])
    if @contactmessage.valid?
      UserMailer.new_contactmessage(@contactmessage).deliver 
      session[:emailstatus] = 1
      respond_to do |format|
  			format.js 
		  end
    else
      session[:emailstatus] = 0
      respond_to do |format|
  			format.js 
		  end
    end
  end


end