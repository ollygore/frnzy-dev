class EmailConfirmsController < ApplicationController

   def new
   end

   def create
   end

   def edit

      @user = User.find_by_email_confirm_token!(params[:id])
      @user.email_confirmed = true
      @enabled = @user.isenabled
      @user.isenabled = '1'
      @user.save
      #Check if the user was already enabled if so then don't login and don't send welcome...
      if @enabled != '1'
         cookies[:auth_token] = @user.auth_token
         @user.send_account_authorised
      end
      redirect_to root_url, :notice => "Welcome to FRNZY."
   end

   def update

      @user = User.find_by_email_confirm_token!(params[:id])
      @user.email_confirmed = true
      @enabled = user.isenabled
      @user.isenabled = '1'
      @user.save
      #Check if the user was already enabled if so then don't login and don't send welcome...
      if @enabled != '1'
         cookies[:auth_token] = @user.auth_token
         @user.send_account_authorised
      end
      @user.send_account_authorised

      #To enable manual authorisation remove the isenabled line above and the instate the following...
      #redirect_to root_url, :notice => "Almost there! We're excited that you've joined us as an exclusive member of FRNZY. We just need to personally approve your account and will send you another message shortly"

   end

end
