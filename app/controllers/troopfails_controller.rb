class TroopfailsController < ApplicationController

   layout 'application'



   def index
      @dealfails = Troopfail.all

      respond_to do |format|
         format.html # index.html.erb
         format.json { render json: @dealfails }
      end
   end




   def show
      @dealfail = Troopfail.find(params[:id])

      respond_to do |format|
         format.html # show.html.erb
         format.json { render json: @dealfail }
      end
   end





   def new
      @dealfail = Troopfail.new

      respond_to do |format|
         format.html # new.html.erb
         format.json { render json: @dealfail }
      end
   end




   def edit
      @dealfail = Troopfail.find(params[:id])
   end





   def create
      @dealfail = Troopfail.new(params[:troopfail])

      respond_to do |format|
         if @dealfail.save
            format.html { redirect_to @dealfail, notice: 'Troopfail was successfully created.' }
            format.json { render json: @dealfail, status: :created, location: @dealfail }
         else
            format.html { render action: "new" }
            format.json { render json: @dealfail.errors, status: :unprocessable_entity }
         end
      end
   end






   def update
      @dealfail = Troopfail.find(params[:id])

      respond_to do |format|
         if @dealfail.update_attributes(params[:troopfail])
            format.html { redirect_to @dealfail, notice: 'Troopfail was successfully updated.' }
            format.json { head :no_content }
         else
            format.html { render action: "edit" }
            format.json { render json: @dealfail.errors, status: :unprocessable_entity }
         end
      end
   end






   def destroy
      @dealfail = Troopfail.find(params[:id])
      @dealfail.destroy

      respond_to do |format|
         format.html { redirect_to troopfails_url }
         format.json { head :no_content }
      end
   end
end
