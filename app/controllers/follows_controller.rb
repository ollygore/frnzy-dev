class FollowsController < ApplicationController

 layout 'application'

  def followuser
  
  @user = User.find(params[:id])


    @followuser = Follow.new()
    @followuser.user_id = params[:id]
    @followuser.follower_id = current_user.id
    @user = User.find(params[:id])

    @follow = Follow.new()
    @follow.follower_id = params[:id]
    @follow.user_id = params[:id]

      respond_to do |format|
        if @followuser.save
          @followsubmitted = true
          format.js
        else
          @followsubmitted = false
          format.js
        end
      end
  end

  def unfollowuser

    @user = User.find(params[:id])
    @follow = Follow.where({:user_id => params[:id], :follower_id => current_user.id}).first
    @follow.destroy

    @follow = Follow.new()
    @follow.follower_id = params[:id]
    @follow.user_id = params[:id]
    
    respond_to do |format|
       format.js
    end
  end

  def following
    @user = User.find(params[:id])
    @follows = Follow.where(:follower_id => @user.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @follows }
    end
  end

  def followers
    @user = User.find(params[:id])
    @follows = Follow.where(:user_id => @user.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @follows }
    end
  end



end
