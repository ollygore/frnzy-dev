class SessionsController < ApplicationController


   layout 'application'


   def new
   end


   def create
      user = User.where('lower(email) = ?', params[:email].downcase).first

      if user && user.authenticate(params[:password])

         if user.role_id == 1
            cookies[:auth_token] = user.auth_token
            #redirect_to session[:return_to], :notice => "Logged in!"
            @ajaxresponse = "Logged in!"

         end

         if user.role_id != 1
            if user.isenabled == 1 && user.email_confirmed == true
               cookies[:auth_token] = user.auth_token
               #redirect_to session[:return_to], :notice => "Logged in!"
                @ajaxresponse = "Logged in!"

            else
               #redirect_to session[:return_to], :notice => "Your account needs to be authorised and your email confirmed before you can use it. Click here to resend your confirmation link."
               @user_id = user.id
               @ajaxresponse = "Confirm Email"
            end
         end

      else
         #redirect_to session[:return_to], :notice => "Invalid email or password"

        @ajaxresponse = "Invalid email or password"

      end


       respond_to do |format|
            format.js
         end

   end



   def destroy
      cookies.delete(:auth_token)
      redirect_to root_url #, :notice => "Logged out!"
   end



   def createfacebook

      begin
        user = User.from_omniauth(env["omniauth.auth"])

        if user.role_id != 1
         if user.isenabled == 1

            cookies[:auth_token] = user.auth_token
            redirect_to root_url, :notice => "Thank you for logging in though Facebook."

         else
            #redirect_to session[:return_to], :notice => "Thank you for joining, your account needs to be authorised before you can use it. You should receive an email within 24 hours."
            user.email_confirmed = true
            user.isenabled = '1'
            user.save
            cookies[:auth_token] = user.auth_token
            user.send_account_authorised

            #Increment accepted referral if referral ID Present
            if cookies[:referral_id] != nil
               ref = Referral.find_by_id(cookies[:referral_id])
               if ref.deal == null && ref.product == null
                  @accRef = AcceptedReferral.new
                  @accRef.referral = ref
                  @accRef.user = user
                  @accRef.save
               end
            end

            redirect_to root_url, :notice => "Welcome to FRNZY."

         end
      end


      rescue
        redirect_to '/', :notice => "Your facebook email address is already registered with the site."
      end

      

   end


end
