class TroopcompletesController < ApplicationController
   layout 'application'






   def show
      @dealcomplete = Troopcomplete.find(params[:id])

      respond_to do |format|
         format.html # show.html.erb
         format.json { render json: @dealcomplete }
      end
   end









   def new
      @dealcomplete = Troopcomplete.new

      respond_to do |format|
         format.html # new.html.erb
         format.json { render json: @dealcomplete }
      end
   end








   def create
      @dealcomplete = Troopcomplete.new(params[:troopcomplete])

      respond_to do |format|
         if @dealcomplete.save
            format.html { redirect_to @dealcomplete, notice: 'Troopcomplete was successfully created.' }
            format.json { render json: @dealcomplete, status: :created, location: @dealcomplete }
         else
            format.html { render action: "new" }
            format.json { render json: @dealcomplete.errors, status: :unprocessable_entity }
         end
      end
   end








   def update
      @dealcomplete = Troopcomplete.find(params[:id])

      respond_to do |format|
         if @dealcomplete.update_attributes(params[:troopcomplete])
            format.html { redirect_to @dealcomplete, notice: 'Troopcomplete was successfully updated.' }
            format.json { head :no_content }
         else
            format.html { render action: "edit" }
            format.json { render json: @dealcomplete.errors, status: :unprocessable_entity }
         end
      end
   end







   def complete
      @dealcomplete = Troopcomplete.find(params[:id])
      @dealcomplete.transaction_status = 1;
      @dealcomplete.save!

      respond_to do |format|
         format.html # index.html.erb
      end

   end





end
