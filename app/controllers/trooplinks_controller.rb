class TrooplinksController < ApplicationController


   layout 'application'


   # respond_to :js


   def new

      @user = User.new

      respond_to do |format|
         format.html # new.html.erb
         format.json { render json: @user }
      end

   end





   def create

      @deallink = Trooplink.create(params[:trooplink])
      @deal = @deallink.troop

      deal = @deal
      deal.popularity = deal.trooplinks.count
      deal.save

      @product = Product.find(@deal.product_id)
      @likes = Like.where(:product_id => @deal.product_id, :user_id => current_user.id)
      @likes.each do |like|
         like.destroy
      end

      render :toggle
   end




   def destroy
      trooplink = Trooplink.find(params[:id]).destroy
      @deal = trooplink.deal

      deal = @deal
      deal.popularity = deal.trooplinks.count
      deal.save

      @product = Product.find(@deal.product_id)
      @likes = Like.create(:product_id => @deal.product_id, :user_id => current_user.id)
      render :toggle
   end

end





