class ApplicationController < ActionController::Base

   protect_from_forgery
   before_filter :populate_admin_count
   before_filter :extract_cart

   def extract_cart
      cart_id = session[:cart_id]
      @cart = session[:cart_id] ? Cart.find(cart_id) : Cart.create
      session[:cart_id] = @cart.id
   end

   def populate_admin_count
      @products_new_count = Product.where("isenabled = '0'").count
      @deals_new_count = Deal.where("isenabled = '0'").count
      @users_new_count = User.where("isenabled = '0'")
      @users_new_count = @users_new_count.where("role_id = '3'").count
      @merchants_new_count = User.where("isenabled = '0'")
      @merchants_new_count = @merchants_new_count.where("role_id = '2'").count
   end

   rescue_from ActiveRecord::RecordNotFound do |e|
      session[:cart_id] = nil
      cookies.delete(:auth_token)
      redirect_to ('/'), notice: ( 'Record not found.' )
      puts "Error during processing: #{$!}"
      puts "Backtrace:\n\t#{e.backtrace.join("\n\t")}"
   end

   private

   def current_user
      @current_user ||= User.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
   end

   helper_method :current_user

end
