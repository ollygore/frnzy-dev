class GroupsController < ApplicationController


   include ActiveMerchant::Billing
   include PaypalExpressHelper


   layout 'application'
   before_filter :check_expirary



   def index
      @groups = Group.all
      @categories = Category.all

      respond_to do |format|
         format.html # index.html.erb
         format.json { render json: @groups }
      end
   end



   def show

      if cookies[:wanted_filter].present?
      else
         cookies[:wanted_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:active_filter].present?
      else
         cookies[:active_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:done_filter].present?
      else
         cookies[:done_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:sort_order].present?
      else
         cookies[:sort_order] = {:value => 'Newest to oldest',:expires => 1.year.from_now,:path => '/'}
      end


      @locations = Location.all
      @categories = Category.all
      @groups = Group.all

      @group = Group.find(params[:id])


      @products = Product.where("isenabled = ? AND group_id = ?", '1', @group.id)
      @deals = Deal.joins(:product).where("group_id = ?", @group.id)


      @users = User.all
      @likes = Like.all


      @deals = @deals.where(:status => ['1','2'])
      if (cookies[:active_filter] == '0') && (cookies[:done_filter] == '0')
         @deals = @deals.where(:status => ['4'])
      elsif (cookies[:active_filter] == '1') && (cookies[:done_filter] == '0')
         @deals = @deals.where(:status => ['1'])
      elsif (cookies[:active_filter] == '0') && (cookies[:done_filter] == '1')
         @deals = @deals.where(:status => ['2'])
      elsif (cookies[:active_filter] == '1') && (cookies[:done_filter] == '1')
         @deals = @deals.where(:status => ['1','2'])
      end

      @deallinks = Trooplink.all


      if cookies[:wanted_filter] == '1'
         @combinedproductsandtroops = (@products + @deals)
      else
         @combinedproductsandtroops = (@deals)
      end


      #SORT BY ELSIFS
      if (cookies[:sort_order] == 'Newest to oldest')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:created_at]}.reverse
      elsif (cookies[:sort_order] == 'Oldest to newest')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:created_at]}
      elsif (cookies[:sort_order] == 'Troops ending soon')
         @combinedproductsandtroops = @deals.where(:status => ['1']).sort_by{|e| e[:auction_end]}
      elsif (cookies[:sort_order] == 'Most expensive')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:price]}.reverse
      elsif (cookies[:sort_order] == 'Cheapest')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:price]}
      elsif (cookies[:sort_order] == 'Popularity')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:popularity]}.reverse
      end



      #ORDER BY ON THIS ARRAY
      @combinedproductsandtroops = @combinedproductsandtroops.paginate(:page => params[:page], :per_page => 15)

      render :layout => "application"

   end





   def new
      @group = Group.new
      @categories = Category.all
      respond_to do |format|
         format.html # new.html.erb
         format.json { render json: @group }
      end
   end




   def edit
      @group = Group.find(params[:id])
      @categories = Category.all

   end






   def create
      @group = Group.new(params[:group])
      @categories = Category.all
      respond_to do |format|
         if @group.save
            format.html { redirect_to admin_categories_path, notice: 'Subcategory was successfully created.' }
         else
            format.html { render action: "new" }
            format.json { render json: @group.errors, status: :unprocessable_entity }
         end
      end
   end






   def update
      @group = Group.find(params[:id])
      @categories = Category.all
      respond_to do |format|
         if @group.update_attributes(params[:group])
            format.html { redirect_to admin_categories_path, notice: 'Subcategory was successfully updated.' }
            format.json { head :no_content }
         else
            format.html { render action: "edit" }
            format.json { render json: @group.errors, status: :unprocessable_entity }
         end
      end
   end





   def destroy
      @group = Group.find(params[:id])
      @group.destroy
      @categories = Category.all
      respond_to do |format|
         format.html { redirect_to admin_categories_path }
         format.json { head :no_content }
      end
   end





   private

   def check_expirary
      @deals_checker = Deal.where("isenabled = ? AND auction_end <= ? AND status = ?", '1', DateTime.now.utc, '1')
      @deals_checker.each do | deal |

         if (deal.trooplinks.count >= deal.troop_limit)
            deal.status = '2';
            deal.end_count = deal.trooplinks.count

            deal.trooplinks.each do |trooplink|
               Troopcomplete.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :transaction_status => 0, :order_id => trooplink.order_id)
               #congratulations email
               @user = User.find(trooplink.user_id)
               @user.send_troop_ended(trooplink)
               trooplink.destroy
            end

         else

            deal.status = '3';

            deal.end_count = deal.trooplinks.count

            if deal.end_count > 0

               deal.trooplinks.each do |trooplink|

                  @gateway ||= PaypalExpressGateway.new(
                  :login => PaypalLogin.login,
                  :password => PaypalLogin.password,
                  :signature => PaypalLogin.signature,
                  )

                  refund = @gateway.transfer(trooplink.order.total * 100, trooplink.order.paypal_email, :currency => 'GBP', :subject => "FRNZY Deal refund", :note => trooplink.deal.product.name)

                  if refund.success?

                     @order = Order.new
                     @order.attributes = {:paypal_email => trooplink.order.paypal_email, :paypal_name => trooplink.order.paypal_name, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  else

                     @order = Order.new
                     @order.attributes = {:paypal_email => refund.message, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  end

                  Troopfail.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :order_id => @order.id)
                  Like.create(:user_id => trooplink.user_id, :product_id => deal.product_id)
                  #failure email
                  @user = User.find(trooplink.user_id)
                  @user.send_troop_failed(trooplink)
                  trooplink.destroy
               end
            end
         end
         deal.save
      end
   end





end
