class UsersController < ApplicationController

   include ActiveMerchant::Billing
   include PaypalExpressHelper

   before_filter :authorize_for_destroy, only: :destroy
   before_filter :authorize_for_edit, only: :edit
   helper_method :sort_column, :sort_direction
   before_filter :check_expirary
   layout 'application'



   def index
      #@users = User.all
      redirect_to '/'
   end



   def view_user
      @roles = Role.all
      @locations = Location.all

      @users = User.search(params[:search]).where("role_id = ?",'3').sort_by{|e| e[:isenabled]}
      if params[:direction] == 'desc'
         @users = @users.sort_by{|e| e[params[:sort]]}.reverse!
      else
         @users = @users.sort_by{|e| e[params[:sort]]}
      end
      @users = @users.paginate(:page => params[:page], :per_page => 50)

   end



   def view_merchant

      @users = User.search(params[:search]).where("role_id = ?",'2').sort_by{|e| e[:isenabled]}
      if params[:direction] == 'desc'
         @users = @users.sort_by{|e| e[params[:sort]]}.reverse!
      else
         @users = @users.sort_by{|e| e[params[:sort]]}
      end
      @users = @users.paginate(:page => params[:page], :per_page => 50)


      @roles = Role.all
      @locations = Location.all
   end




   def dashboard
      if current_user

         if current_user.role_id == 1
            redirect_to '/admin_deals'
         else



         @user = User.find(current_user.id)
         @likes = Like.where('likes.user_id' =>  current_user.id)
         @products = Product.joins(:likes).where('likes.user_id' =>  current_user.id)
         @roles = Role.all
         @locations = Location.all
         @deallinks = Trooplink.where('trooplinks.user_id' =>  current_user.id)
         @usersorders = Order.where('user_id' =>  current_user.id)
         if current_user.role_id == 3
            @usertroopslive = Deal.joins(:trooplinks).where('trooplinks.user_id' =>  current_user.id)
            @usertroopslive = @usertroopslive.where("isenabled = '1'")
            @usertroopslive = @usertroopslive.where("status = '1'")

            @usertroopscompleted = Deal.joins(:troopcompletes).where('troopcompletes.user_id' =>  current_user.id)
            @usertroopscompleted = @usertroopscompleted.where("isenabled = '1'")
            @usertroopscompleted = @usertroopscompleted.where("status = '2'")

            @usertroopsfailed = Deal.joins(:troopfails).where('troopfails.user_id' =>  current_user.id)
            @usertroopsfailed = @usertroopsfailed.where("isenabled = '1'")
            @usertroopsfailed = @usertroopsfailed.where("status = '3'")

         end

         if current_user.role_id == 2
            @merchanttroopslive = Deal.where("user_id = ? AND isenabled = ? AND status = ?", current_user.id,'1','1')
            @merchanttroopssuccess = Deal.where("user_id = ? AND isenabled = ? AND status = ?", current_user.id,'1','2')
            @merchanttroopsfailed = Deal.where("user_id = ? AND isenabled = ? AND status = ?", current_user.id,'1','3')
         end

         respond_to do |format|
            format.html # show.html.erb
            format.json { render json: @user }
         end

      end
      
      end
   end





   def show
      
      #needed for all types
      @user = User.find(params[:id])
      @likes = Like.where('likes.user_id' =>  @user.id)
      @roles = Role.all
      @locations = Location.all


      #full stream for your profile
      if current_user 
         if current_user == @user
            
            productsforstream = Product.joins(:likes).where('likes.user_id' =>  @user.id)

            if @user.role_id == 2
               troopsforstream = Deal.where('user_id' =>  @user.id)
            else
               livetroops = Deal.joins(:trooplinks).where('trooplinks.user_id' =>  @user.id)
               completedtroops = Deal.joins(:troopcompletes).where('troopcompletes.user_id' =>  @user.id)
               troopsforstream = livetroops + completedtroops
            end

            @yourstream = troopsforstream + productsforstream

         end
      end

      #viewing someone else profile
      if @yourstream.nil?

         @products = Product.joins(:likes).where('likes.user_id' =>  @user.id)

         #live user shopping list and troops
         if @user.role_id == 3    
            @deals = Deal.joins(:trooplinks).where('trooplinks.user_id' =>  @user.id)
         end

         #live merchant troops + merchant wants
         if @user.role_id == 2
            @deals = Deal.where("user_id = ? AND isenabled = ? AND status = ?", @user.id,'1','1')
         end

         #live admin shopping list and troops
         if @user.role_id == 1    
            @deals = Deal.joins(:trooplinks).where('trooplinks.user_id' =>  @user.id)
         end

         @combinedproductsandtroops = @deals + @products

      end


      #for admin to view users and merchants data
      if current_user
         if current_user.role_id == 1
            if @user.role_id == 2
               @merchanttroopslive = Deal.where("user_id = ? AND isenabled = ? AND status = ?", @user.id,'1','1')
               @merchanttroopssuccess = Deal.where("user_id = ? AND isenabled = ? AND status = ?", @user.id,'1','2')
               @merchanttroopsfailed = Deal.where("user_id = ? AND isenabled = ? AND status = ?", @user.id,'1','3')
            end
            if @user.role_id == 3
               @usertroopslive = Deal.joins(:trooplinks).where('trooplinks.user_id' =>  @user.id)
               @usertroopslive = @usertroopslive.where("isenabled = '1'")
               @usertroopslive = @usertroopslive.where("status = '1'")
               @usertroopscompleted = Deal.joins(:troopcompletes).where('troopcompletes.user_id' =>  @user.id)
               @usertroopscompleted = @usertroopscompleted.where("isenabled = '1'")
               @usertroopscompleted = @usertroopscompleted.where("status = '2'")
               @usertroopsfailed = Deal.joins(:troopfails).where('troopfails.user_id' =>  @user.id)
               @usertroopsfailed = @usertroopsfailed.where("isenabled = '1'")
               @usertroopsfailed = @usertroopsfailed.where("status = '3'")       
            end
         end
      end

      render :layout => "application"

   end



  
   def new
      @locations = Location.all

      @user = User.new
      @roles = Role.all

      respond_to do |format|
         format.html # new.html.erb
         format.json { render json: @user }
      end
   end



   def edit
      @user = User.find(params[:id])
      @roles = Role.all
      @locations = Location.all

   end



   def create
      @user = User.new(params[:user])
      @roles = Role.all
      @locations = Location.all

      if @user.role_id == 1
         @user.isenabled = 1
      end


      if @user.save
         session[:user_id] = @user.id
         session[:registerstatus] = 1

         @user.send_email_confirm

         if session[:referral_id] != nil
            ref = Referral.find(session[:referral_id])
            if ref.deal == nil
               @accRef = AcceptedReferral.new
               @accRef.referral = ref
               @accRef.user = @user
               @accRef.save
            end
         end

         respond_to do |format|
            format.js
            format.html {redirect_to ('/'), notice: ( 'Your account needs to be authorised and email verified before you can use it. Please check your email and click the confirmation link.  Thank you.' )}
         end
      else
         session[:registerstatus] = 0
         respond_to do |format|
            format.js
         end
      end
   end



   def update
      @user = User.find(params[:id])
      @roles = Role.all
      @locations = Location.all
      respond_to do |format|
         if @user.update_attributes(params[:user])
            if params[:user][:isenabled].nil?
            else
               @user.send_account_authorised
            end

            if @user.role_id == 3

               if current_user.role_id == 3
                  format.html { redirect_to userprofile_path, notice: 'User was successfully updated.' }
               else
                  format.html { redirect_to admin_users_path, notice: 'User was successfully updated.' }
               end

            elsif  @user.role_id == 2

               if current_user.role_id == 2
                  format.html { redirect_to userprofile_path, notice: 'User was successfully updated.' }
               else
                  format.html { redirect_to admin_merchants_path, notice: 'User was successfully updated.' }
               end

            elsif  @user.role_id == 1

               if current_user.role_id == 1
                  format.html { redirect_to userprofile_path, notice: 'Admin details successfully updated.' }
                else
                  format.html { redirect_to userprofile_path, notice: 'User was successfully updated.' }
               end

            end
            
            format.json { head :no_content }
         else
            format.html { render action: "edit" }
            format.json { render json: @user.errors, status: :unprocessable_entity }
         end
      end
   end

   
   def resend_confirmation_email

      @user = User.find_by_id(params[:id])
      
      if @user == nil
         @ajaxresponse = 'Error please contact support.'
      else
         if @user.email_confirmed == true
            @ajaxresponse = 'Your email is already verified.'
         else
            @user.send_email_confirm
            @ajaxresponse = 'Thank you. Please check your email.'
         end
      end

      respond_to do |format|
         format.js
      end

   end


   def destroy

      @user = User.find(params[:id])

      @followsfollower = Follow.where(:follower_id => @user.id)
      @followsfollower.delete_all

      @followsuser = Follow.where(:user_id => @user.id)
      @followsuser.delete_all

      @user.destroy

      respond_to do |format|
         format.html { redirect_to '/admin_users', notice: 'Delete successful.' }
         format.json { head :no_content }
      end
   end


   private


   def sort_column
      User.column_names.include?(params[:sort]) ? params[:sort] : "isenabled"
   end

   def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
   end


   def check_expirary
      @deals_checker = Deal.where("isenabled = ? AND auction_end <= ? AND status = ?", '1', DateTime.now.utc, '1')
      @deals_checker.each do | deal |

         if (deal.trooplinks.count >= deal.troop_limit)
            deal.status = '2';
            deal.end_count = deal.trooplinks.count

            deal.trooplinks.each do |trooplink|
               Troopcomplete.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :transaction_status => 0, :order_id => trooplink.order_id)
               #congratulations email
               @user = User.find(trooplink.user_id)
               @user.send_troop_ended(trooplink)
               trooplink.destroy
            end

         else

            deal.status = '3';

            deal.end_count = deal.trooplinks.count

            if deal.end_count > 0

               deal.trooplinks.each do |trooplink|

                  @gateway ||= PaypalExpressGateway.new(
                  :login => PaypalLogin.login,
                  :password => PaypalLogin.password,
                  :signature => PaypalLogin.signature,
                  )

                  #refund = @gateway.transfer(trooplink.order.total * 100, trooplink.order.paypal_email, :currency => 'GBP', :subject => "FRNZY refund", :note => trooplink.deal.product.name)
                  refund = @gateway.refund nil, trooplink.order.transactionid

                  if refund.success?

                     @order = Order.new
                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => trooplink.order.paypal_email, :paypal_name => trooplink.order.paypal_name, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  else

                     @order = Order.new
                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => refund.message, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  end

                  Troopfail.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :order_id => @order.id)
                  Like.create(:user_id => trooplink.user_id, :product_id => deal.product_id)
                  #failure email
                  @user = User.find(trooplink.user_id)
                  @user.send_troop_failed(trooplink)
                  trooplink.destroy
               end
            end
         end
         deal.save
      end
   end



   def authorize_for_destroy
      if current_user
         if current_user.role_id != 1
            redirect_to root_url, notice: 'not authorised.'
         end
      else
         redirect_to root_url, notice: 'not authorised.'
      end
   end


   def authorize_for_edit
      @user = User.find(params[:id])
      if current_user
         if current_user.role_id == 1
            #admin gets access to everyones
         else
            #users can only edit there own profile
            if current_user.username != @user.username
               redirect_to root_url, notice: 'not authorised.'
            end
         end
      else
         redirect_to root_url, notice: 'not authorised.'
      end
   end

end
