class ProductsController < ApplicationController

   include ActiveMerchant::Billing
   include PaypalExpressHelper

   before_filter :check_expirary
   helper_method :sort_column, :sort_direction

   layout 'application'

   def index
      raise ActionController::RoutingError.new('No such page')
   end


   def share

      @product = Product.find(params[:id])
      respond_to do |format|
         format.js
      end

   end

   def invite
      @product = Product.find(params[:id])
      @user = current_user
      if Referral.uniq.exists?(:user_id => @user.id, :product_id => @product.id)  
         @referral = Referral.where(:user_id => @user.id, :product_id => @product.id).first
      else
         @referral = Referral.new
         @referral.referral_code = rand(36**9).to_s(36)
         @referral.product = @product
         @referral.user = @user
         @referral.save
      end
      respond_to do |format|
         format.js
      end
   end

   
   def homepage
      
      #if current_user
         #if current_user.email = 'olly@goreblimey.com'
         #user = User.find_by_email('jezmurrell@gmail.com')
         #user = User.find_by_email('olly@goreblimey.com')
         #user = User.find_by_email('js@eszconsulting.com')
         #user.delay.send_responder_mail_1
         #user.delay.send_responder_mail_2
         #user.delay.send_responder_mail_3
         #user.delay.send_responder_mail_4
         #user.delay.send_responder_mail_5
         #user.delay.send_responder_mail_6
         #end
      #end

      #require 'date'
      #date = DateTime.parse("2014-11-27")
      #users = User.where("created_at < ?", date) 
      #users.count #74 people

      #sent the 1st email Wednesday 3pm
      #send the 2nd email Saturday 3pm
      #send the 3rd email Tuesday 3pm
      #send the 4th email Friday 3pm
      #send the 5th email Friday 3pm

      #users.each do |user|
      #   user.delay.send_responder_mail_4
      #end

      #end

      #AUTORESPONDER START
      #AUTORESPONDER START
      #AUTORESPONDER START
      #s = Setting.first
      #if s.lastrun != DateTime.now.to_date
      #   past18days = User.where(created_at: ((DateTime.now-18.day).beginning_of_day)..((DateTime.now-2.day).beginning_of_day))
      #   registerd_1_day_ago = past18days.where(created_at: ((DateTime.now-3.day).beginning_of_day)..((DateTime.now-2.day).beginning_of_day))
      #   registerd_2_days_ago = past18days.where(created_at: ((DateTime.now-6.day).beginning_of_day)..((DateTime.now-5.day).beginning_of_day))
      #   registerd_3_days_ago = past18days.where(created_at: ((DateTime.now-9.day).beginning_of_day)..((DateTime.now-8.day).beginning_of_day))
      #   registerd_4_days_ago = past18days.where(created_at: ((DateTime.now-12.day).beginning_of_day)..((DateTime.now-11.day).beginning_of_day))
      #   registerd_5_days_ago = past18days.where(created_at: ((DateTime.now-15.day).beginning_of_day)..((DateTime.now-14.day).beginning_of_day))
      #   registerd_6_days_ago = past18days.where(created_at: ((DateTime.now-18.day).beginning_of_day)..((DateTime.now-17.day).beginning_of_day))
      #   registerd_1_day_ago.each do |user|
      #      user.delay.send_responder_mail_1
      #   end
      #   registerd_2_days_ago.each do |user|
      #      user.delay.send_responder_mail_2
      #   end
      #   registerd_3_days_ago.each do |user|
      #      user.delay.send_responder_mail_3
      #   end
      #   registerd_4_days_ago.each do |user|
      #      user.delay.send_responder_mail_4
      #   end
      #   registerd_5_days_ago.each do |user|
      #      user.delay.send_responder_mail_5
      #   end
      #   registerd_6_days_ago.each do |user|
      #      user.delay.send_responder_mail_6
      #   end
      #   s.lastrun = (DateTime.now).to_date
      #   s.save   
      #end
      #AUTORESPONDER END
      #AUTORESPONDER END
      #AUTORESPONDER END


      if cookies[:wanted_filter].present?
      else
         cookies[:wanted_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:active_filter].present?
      else
         cookies[:active_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end

      if cookies[:done_filter].present?
      else
         cookies[:done_filter] = {:value => '1',:expires => 1.year.from_now,:path => '/'}
      end


      if cookies[:sort_order].present?
      else
         cookies[:sort_order] = {:value => 'Most recent',:expires => 1.year.from_now,:path => '/'}
      end

      @products = Product.where("isenabled = '1'")
      @deals = Deal.where("isenabled = '1'")

      @users = User.all
      @likes = Like.all

      if current_user != nil
         userProducts = Product.where(:isenabled => '0',:user_id => current_user.id)
         @products = userProducts + @products
      end
      @deals = @deals.where(:status => ['1','2'])
      if (cookies[:active_filter] == '0') && (cookies[:done_filter] == '0')
         @deals = @deals.where(:status => ['4'])
      elsif (cookies[:active_filter] == '1') && (cookies[:done_filter] == '0')
         @deals = @deals.where(:status => ['1'])
      elsif (cookies[:active_filter] == '0') && (cookies[:done_filter] == '1')
         @deals = @deals.where(:status => ['2'])
      elsif (cookies[:active_filter] == '1') && (cookies[:done_filter] == '1')
         @deals = @deals.where(:status => ['1','2'])
      end

      @deallinks = Trooplink.all
      @locations = Location.all
      @categories = Category.all
      @groups = Group.all

      if cookies[:wanted_filter] == '1'
         @combinedproductsandtroops = (@deals + @products)
      else
         @combinedproductsandtroops = (@deals)
      end

      #TESTING
      #@combinedproductsandtroops =  @products


      #SORT BY ELSIFS
      if (cookies[:sort_order] == 'Most recent')
         # @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:created_at]}.reverse
      elsif (cookies[:sort_order] == 'Oldest')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:created_at]}
      elsif (cookies[:sort_order] == 'Troops ending soon')
         @combinedproductsandtroops = @deals.where(:status => ['1']).sort_by{|e| e[:auction_end]}
      elsif (cookies[:sort_order] == 'Most expensive')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:price]}.reverse
      elsif (cookies[:sort_order] == 'Best deals')
         @combinedproductsandtroops = @deals.where(:status => ['1'])#@combinedproductsandtroops.sort_by{|e| e[:price]}
      elsif (cookies[:sort_order] == 'Most popular')
         @combinedproductsandtroops = @combinedproductsandtroops.sort_by{|e| e[:popularity]}.reverse
      end

      @combinedproductsandtroops = @combinedproductsandtroops.paginate(:page => params[:page], :per_page => 15)

      respond_to do |format|
         format.html {render :layout => "application"}
         format.js
      end


   end




   def view
      #used for admin products view
      @products = Product.all
      @groups = Group.all


      @products = Product.search(params[:search]).sort_by{|e| e[:created_at]}

      if params[:direction] == 'desc'
         @products = @products.sort_by{|e| e[params[:sort]]}
      else
         @products = @products.sort_by{|e| e[params[:sort]]}.reverse!
      end
      @products = @products.paginate(:page => params[:page], :per_page => 30)

      @users = User.all
      @categories = Category.all
      respond_to do |format|
         format.html # index.html.erb
         format.json { render json: @products }
      end
   end




   def show

      @product = Product.find(params[:id])
      @product.popularity = (@product.likes.count)
      @product.save


      @deal = Deal.all
      @categories = Category.all
      @users = User.all
      @comments = @product.comments.recent.limit(10).reverse
      
      render :layout => "application"

      #@product.comments.create(:title => "Second comment.", :comment => "This is the second comment.", :user_id => current_user.id)

   end



   def add_new_comment

      product = Product.find(params[:id])
      product.comments.create(params[:comment])
      redirect_to :action => :show, :id => product

   end





    def remove_comment

      @comment = Comment.find(params[:id])
      begin
         if (current_user.id == @comment.user_id) || (current_user.role_id == 1)
            @comment.destroy
         end
         respond_to do |format|
            format.js
         end
      rescue 
         redirect_to '/'
      ensure
      end
      
   end





   def new
      @product = Product.new
      @categories = Category.all

      respond_to do |format|
         format.html # new.html.erb
         format.json { render json: @product }
      end
   end

   def duplicate
      #TODO: Check if name already exists, if it does find that product.
      @product_name = params[:name]
      @results = Product.fuzzy(@product_name)

      respond_to do |format|
         format.js
      end
   end

   def edit
      @categories = Category.all
      @groups = Group.all
      @product = Product.find(params[:id])
   end

   def like_and_view
      @product = Product.find(params[:id])
      @like = Like.create({:product_id => @product.id, :user_id => current_user.id})

      redirect_to "/products/#{@product.slug}"
   end




   def create
      @product = Product.new(params[:product])
      @categories = Category.all

      if @product.group == nil
         group = Group.where("name = ? AND category_id = ?", 'Other', @product.category_id).first
         puts group.inspect
         if group != nil
            @product.group = group
         end
      end
      if @product.image_url != ''
         @product.product_image = URI.parse(@product.image_url)
      end

      @product.popularity = 0

      respond_to do |format|

         if @product.save

            if current_user.role_id == 1

               format.html { redirect_to admin_products_path, notice: 'Product was successfully created.' }
               format.json { render json: @product, status: :created, location: @product }

            else

               format.html { redirect_to root_url, notice: "Your product has been submitted. Once it's been approved you can share it and invite people to want it too!" }
               format.json { render json: @product, status: :created, location: @product }

            end

         else
            format.html { render action: "new" }
            format.json { render json: @product.errors, status: :unprocessable_entity }
         end
      end
   end






   def update
      @product = Product.find(params[:id])
      @categories = Category.all


      if @product.isenabled == 0 && params[:product][:isenabled] == '1' && @product.user.role_id != 1

         @like = Like.create({:product_id => @product.id, :user_id => @product.user.id})

      end

      respond_to do |format|
         if @product.update_attributes(params[:product])
            format.html { redirect_to admin_products_path, notice: 'Product was successfully updated.' }
            format.json { head :no_content }
         else
            format.html { render action: "edit" }
            format.json { render json: @product.errors, status: :unprocessable_entity }
         end
      end
   end





   def destroy
      @product = Product.find(params[:id])
      @product.destroy

      respond_to do |format|
         format.html { redirect_to admin_products_path }
         format.json { head :no_content }
      end
   end






   private


   def sort_column
      Product.column_names.include?(params[:sort]) ? params[:sort] : "isenabled"
   end

   def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
   end



   def check_expirary
      @deals_checker = Deal.where("isenabled = ? AND auction_end <= ? AND status = ?", '1', DateTime.now.utc, '1')
      @deals_checker.each do | deal |

         if (deal.trooplinks.count >= deal.troop_limit)
            deal.status = '2';
            deal.end_count = deal.trooplinks.count

            deal.trooplinks.each do |trooplink|
               Troopcomplete.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :transaction_status => 0, :order_id => trooplink.order_id)
               #congratulations email
               @user = User.find(trooplink.user_id)
               @user.send_troop_ended(trooplink)
               trooplink.destroy
            end

         else

            deal.status = '3';

            deal.end_count = deal.trooplinks.count

            if deal.end_count > 0

               deal.trooplinks.each do |trooplink|

                  @gateway ||= PaypalExpressGateway.new(
                  :login => PaypalLogin.login,
                  :password => PaypalLogin.password,
                  :signature => PaypalLogin.signature,
                  )

                  #refund = @gateway.transfer(trooplink.order.total * 100, trooplink.order.paypal_email, :currency => 'GBP', :subject => "FRNZY refund", :note => trooplink.deal.product.name)
                  refund = @gateway.refund nil, trooplink.order.transactionid

                  if refund.success?

                     @order = Order.new
                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => trooplink.order.paypal_email, :paypal_name => trooplink.order.paypal_name, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  else

                     @order = Order.new
                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => refund.message, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  end

                  Troopfail.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :order_id => @order.id)
                  Like.create(:user_id => trooplink.user_id, :product_id => deal.product_id)
                  #failure email
                  @user = User.find(trooplink.user_id)
                  @user.send_troop_failed(trooplink)
                  trooplink.destroy
               end
            end
         end
         deal.save
      end
   end





end
