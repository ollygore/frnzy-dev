class ScrapeController < ApplicationController

	def scrape
		@imglist = Array.new

		def checkDimensions (imageSrc)
			noPath = nil

	 		if imageSrc.starts_with?('/')
	 			if imageSrc.starts_with?('//') 
	 				url = @url.scheme + ":" + imageSrc
	 			else
		 			url = @url.scheme + "://" + @url.host() + imageSrc
		 		end
	 		elsif imageSrc.starts_with?('http')
	 			url = imageSrc
	 		else
	 			path = @url.path.to_s.sub(File.basename(@url.path),'')
	 			url = @url.scheme + "://" + @url.host() + path + imageSrc
	 			noPath = url = @url.scheme + "://" + @url.host() + "/" + imageSrc
	 		end
	 		#Strip Query -- this is useful for some url's however others use the query to get an image..  DON'T USE WITHOUT A BACKUP METHOD....
	 		#url = URI.parse(url)
	 		#url.query = nil
	 		#url = url.to_s
	 		#end strip query...

	 		if url != ""
			 	Rails.logger.debug("URL: #{url.inspect}")
		 		begin
			 		dimensions = FastImage.size(URI.escape(url))
			 		if dimensions == nil and noPath != nil
			 			dimensions = FastImage.size(URI.escape(noPath))
			 		end
			 		Rails.logger.debug("Dimensions: #{dimensions.inspect}")
			 		if dimensions != nil
				 		if dimensions[0] > 300 and dimensions[1] > 300
				 			#If the image is high res then add it to the front of the list to prioritise the users selection.
				 			@imglist.unshift(url)
				 		elsif dimensions[0] > 140 or dimensions[1] > 140
				 			@imglist.push(url)
				 		end
				 	else
				 		#@imglist.push(url)
				 	end
				rescue
					#Do nothing..
					Rails.logger.debug("Dimensions: Error!!")
			 	end
			end
		end


		begin
			@url = URI.parse(params[:url])
			@doc = Nokogiri::HTML(open(params[:url]))	

		 	#Rails.logger.debug("DOC: #{open(params[:url]).base_uri.to_s.inspect}")

			@imgsFound = @doc.css('img')

			#Pull in an OG:Image if it exists....
			ogImage = @doc.css('meta[property="og:image"]')
			if ogImage != nil and ogImage[0] != nil
				Rails.logger.debug("ogImage: #{ogImage[0]['content'].inspect}")
				if ogImage[0]['content'] != nil
					checkDimensions(ogImage[0]['content'])
				end
			end

		 	@imgsFound.each do |image|
		 		url = ''
		 		if image['src'] != nil
			 		checkDimensions(image['src'])
				end

				if image['data-original'] != nil
					checkDimensions(image['data-original'])
				end
			end 

			if @imglist.count == 0
				@error = "The URL enterred does not appear to have useable images.  Please try a different URL"
			end
		rescue URI::InvalidURIError => er
			puts "Invalid URL Error: #{er}"
			@error = "Oops! This is an invalid URL. Please check it and try again.  If you are still having problems email us the name of the product and we will add it for you."
		rescue => er
			puts "General Error: #{er}"
			@error = "Oops! An error has occurred. Please check the URL and try again.  If you are still having problems email us the name of the product and we will add it for you."
		end
      
		respond_to do |format|
			format.js
		end
   end
end
