class DealsController < ApplicationController

   include ActiveMerchant::Billing
   include PaypalExpressHelper

   layout "troop", :only => [ :modalshow ]
   helper_method :sort_column, :sort_direction
   before_filter :check_expirary

   layout 'application'

   def index
      raise ActionController::RoutingError.new('No such page')
   end

   def share
      @deal = Deal.find(params[:id])
      respond_to do |format|
         format.js
      end
   end

   def invite
      @deal = Deal.find(params[:id])
      @user = current_user
      if Referral.uniq.exists?(:user_id => @user.id, :deal_id => @deal.id)  
         @referral = Referral.where(:user_id => @user.id, :deal_id => @deal.id).first
      else
         @referral = Referral.new
         @referral.referral_code = rand(36**10).to_s(36)
         @referral.deal = @deal
         @referral.user = @user
         @referral.save
      end
      respond_to do |format|
         format.js
      end
   end

   def view

      if params[:search].nil?
         @deals = Deal.where("status = ? AND isenabled = ?", '1', '1')
      else
         @deals = Deal.search(params[:search])
         @deals = @deals.where(:status => ['1'])
         @deals = @deals.where(:isenabled => ['1'])
      end
      if params[:direction] == 'desc'
         @deals = @deals.sort_by{|e| e[params[:sort]]}.reverse!
      else
         @deals = @deals.sort_by{|e| e[params[:sort]]}
      end
      @deals = @deals.paginate(:page => params[:page_live], :per_page => 10)

      if params[:search].nil?
         @dealscompleted = Deal.where("status = ? AND isenabled = ?", '2', '1')
      else
         @dealscompleted = Deal.search(params[:search])
         @dealscompleted = @dealscompleted.where(:status => ['2'])
         @dealscompleted = @dealscompleted.where(:isenabled => ['1'])
      end

      if params[:direction] == 'desc'
         @dealscompleted = @dealscompleted.sort_by{|e| e[params[:sort]]}.reverse!
      else
         @dealscompleted = @dealscompleted.sort_by{|e| e[params[:sort]]}
      end
      @dealscompleted = @dealscompleted.paginate(:page => params[:page_completed], :per_page => 10)

      if params[:search].nil?
         @dealsexpired = Deal.where("status = ? AND isenabled = ?", '3', '1')
      else
         @dealsexpired = Deal.search(params[:search])
         @dealsexpired = @dealsexpired.where(:status => ['3'])
         @dealsexpired = @dealsexpired.where(:isenabled => ['1'])
      end

      if params[:direction] == 'desc'
         @dealsexpired = @dealsexpired.sort_by{|e| e[params[:sort]]}.reverse!
      else
         @dealsexpired = @dealsexpired.sort_by{|e| e[params[:sort]]}
      end
      @dealsexpired = @dealsexpired.paginate(:page => params[:page_expired], :per_page => 10)

      if params[:search].nil?
         @dealswaiting = Deal.where("isenabled = ?", '0')
      else
         @dealswaiting = Deal.search(params[:search])
         @dealswaiting = @dealswaiting.where(:isenabled => ['0'])
      end

      if params[:direction] == 'desc'
         @dealswaiting = @dealswaiting.sort_by{|e| e[params[:sort]]}.reverse!
      else
         @dealswaiting = @dealswaiting.sort_by{|e| e[params[:sort]]}
      end
      @dealswaiting = @dealswaiting.paginate(:page => params[:page_waiting], :per_page => 10)

      @users = User.all
      @products = User.all
      @locations = Location.all

      respond_to do |format|
         format.html # index.html.erb
         format.json { render json: @deals }
      end
   end


   def show

      @products = Product.all
      @deal = Deal.find(params[:id])
      @locations = Location.all
      @successfulusers = User.all(:joins => :troopcompletes, :conditions => {:troopcompletes => {:deal_id => @deal.id}})
      @currenttroopers = User.all(:joins => :trooplinks, :conditions => {:trooplinks => {:deal_id => @deal.id}})
      @comments = @deal.comments.recent.limit(10).reverse

      render :layout => "application"
   end


   def new
      @product = Product.find(params[:id])
      @deal = Deal.new
      @products = Product.all
      @users = User.all
      @locations = Location.all
      respond_to do |format|
         format.html # new.html.erb
         format.json { render json: @deal }
      end
   end


   def add_new_comment

      deal = Deal.find(params[:id])
      deal.comments.create(params[:comment])
      redirect_to :action => :show, :id => deal

   end


   def modalshow
      @deal = Deal.find(params[:id])
      @locations = Location.all

      respond_to do |format|
         format.html # show.html.erb
         format.json { render json: @deal }
      end
   end


   def edit 
      @deal = Deal.find(params[:id])
      @locations = Location.all
   end


   def create
      @deal = Deal.new(params[:deal])

      @product = Product.find(@deal.product_id)

      respond_to do |format|
         if @deal.save
            @dealsaved = true
            format.html { redirect_to '/', notice: ("Thank you for submitting this offer for " + @deal.product.name + ". Once it's been confirmed we'll post it on FRNZY.") }
           format.js
         else
          @dealsaved = false
          format.html { render action: "new" }
          format.js
         end
      end
   end


   def update
      @deal = Deal.find(params[:id])
      @locations = Location.all

      respond_to do |format|
         if @deal.update_attributes(params[:deal])
            
            if @deal.previous_changes.key? "isenabled"
             if @deal.isenabled == 1
               @deal.send_want_emails
             end
            end

            format.html { redirect_to admin_deals_path, notice: 'Group has been successfully updated.' }
            format.json { head :no_content }
         else
            format.html { render action: "edit" }
            format.json { render json: @deal.errors, status: :unprocessable_entity }
         end
      end
   end

   def destroy
      @deal = Deal.find(params[:id])
      @deal.destroy

      respond_to do |format|
         format.html { redirect_to admin_deals_path }
         format.json { head :no_content }
      end
   end


   private


   def sort_column
      Product.column_names.include?(params[:sort]) ? params[:sort] : "auction_end"
   end

   def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
   end

   def check_expirary
      @deals_checker = Deal.where("isenabled = ? AND auction_end <= ? AND status = ?", '1', DateTime.now.utc, '1')
      @deals_checker.each do | deal |

         if (deal.trooplinks.count >= deal.troop_limit)
            deal.status = '2';
            deal.end_count = deal.trooplinks.count

            deal.trooplinks.each do |trooplink|
               Troopcomplete.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :transaction_status => 0, :order_id => trooplink.order_id)
               #congratulations email
               @user = User.find(trooplink.user_id)
               @user.send_troop_ended(trooplink)
               trooplink.destroy
            end

         else

            deal.status = '3';

            deal.end_count = deal.trooplinks.count

            if deal.end_count > 0

               deal.trooplinks.each do |trooplink|

                  @gateway ||= PaypalExpressGateway.new(
                  :login => PaypalLogin.login,
                  :password => PaypalLogin.password,
                  :signature => PaypalLogin.signature,
                  )

                  #refund = @gateway.transfer(trooplink.order.total * 100, trooplink.order.paypal_email, :currency => 'GBP', :subject => "FRNZY refund", :note => trooplink.deal.product.name)
                  refund = @gateway.refund nil, trooplink.order.transactionid

                  if refund.success?

                     @order = Order.new
                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => trooplink.order.paypal_email, :paypal_name => trooplink.order.paypal_name, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  else

                     @order = Order.new
                     @order.attributes = {:transactionid =>  trooplink.order.transactionid, :paypal_email => refund.message, :user_id => trooplink.user_id, :total => trooplink.order.total, :order_type => 1, :deal_id => trooplink.deal_id }
                     @order.save!

                  end

                  Troopfail.create(:user_id => trooplink.user_id, :deal_id => trooplink.deal_id, :order_id => @order.id)
                  Like.create(:user_id => trooplink.user_id, :product_id => deal.product_id)
                  #failure email
                  @user = User.find(trooplink.user_id)
                  @user.send_troop_failed(trooplink)
                  trooplink.destroy
               end
            end
         end
         deal.save
      end
   end

end
