class LikesController < ApplicationController


   def index
      @likes = Like.all

      respond_to do |format|
         format.html # index.html.erb
         format.json { render json: @likes }
      end
   end


   def create
      @like = Like.create(params[:like])
      @product = @like.product
      product = @like.product
      product.popularity = product.likes.count
      product.save
      render :toggle
   end




   def destroy

      like = Like.find(params[:id])
      
      @product = like.product
      @product.popularity = (@product.likes.count-1)
      @product.save

      like.destroy

      respond_to do |format|
         #format.html { redirect_to current_user, notice: 'Your have removed your love for this item.' }
         format.js 
      end
   end

end
