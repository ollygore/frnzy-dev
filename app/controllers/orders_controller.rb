class OrdersController < ApplicationController


   layout 'application'
   helper_method :sort_column, :sort_direction


   def view
      #used for admin orders view

      @orders = Order.search(params[:search])

      if params[:direction] == 'desc'
         @orders = @orders.sort_by{|e| e[params[:sort]]}.reverse!
      else
         @orders = @orders.sort_by{|e| e[params[:sort]]}
      end
      @orders = @orders.paginate(:page => params[:page], :per_page => 30)

      respond_to do |format|
         format.html # index.html.erb
         format.json { render json: @orders }
      end

   end


   def sort_column
      Order.column_names.include?(params[:sort]) ? params[:sort] : "isenabled"
   end



   def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
   end


end
