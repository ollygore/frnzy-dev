class CartsController < ApplicationController

   def create
      @deal = Deal.find(params[:deal_id])
      @cart.clear
      @cart.add(@deal, @deal.price)
      redirect_to cart_path
   end

   def destroy
      @deal = Deal.find(params[:deal_id])
      @cart.remove(@deal, 1)
      redirect_to cart_path
   end

   def show
      render :layout => "application"
   end

end
