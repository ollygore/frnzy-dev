class Troopcomplete < ActiveRecord::Base
 
  belongs_to :user
  belongs_to :deal
  belongs_to :order
  
  attr_accessible :deal_id, :user_id, :transaction_status, :order_id
  validates_uniqueness_of :deal_id, :scope => :user_id



end
