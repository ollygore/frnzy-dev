class Referral < ActiveRecord::Base
  belongs_to :user
  belongs_to :product
  belongs_to :deal
  attr_accessible :email_addr, :referral_code

  has_many :accepted_referral
	
end
