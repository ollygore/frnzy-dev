class Group < ActiveRecord::Base

   extend FriendlyId
   friendly_id :slug
   has_many :products
   belongs_to :category

   validates_presence_of :name
   validates_presence_of :description
   validates_presence_of :slug
   validates_presence_of :category_id
   validates_uniqueness_of :slug


   accepts_nested_attributes_for :category
   accepts_nested_attributes_for :products
   attr_accessible :category_id, :description, :name, :slug


end




