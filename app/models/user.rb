class User < ActiveRecord::Base


   before_create { generate_token(:auth_token) }
   
   default_scope order('created_at ASC')

   #ROLES
   # 1 = admin

   extend FriendlyId
   friendly_id :username

   belongs_to :role
   has_many :likes, :dependent => :destroy
   has_many :trooplinks, :dependent => :destroy
   belongs_to :location
   has_many :troopcompletes, :dependent => :destroy
   has_many :orders, :dependent => :destroy
   has_many :troopfails, :dependent => :destroy
   has_many :follows, :dependent => :destroy
   has_many :referrals
   has_many :accepted_referral

   accepts_nested_attributes_for :likes
   accepts_nested_attributes_for :trooplinks
   accepts_nested_attributes_for :troopcompletes
   accepts_nested_attributes_for :troopfails

   has_attached_file :photo, :styles => { :extrasmall => "100x100#",:small => "200x200#", :medium => "500x420#", :largebox => "356x356#" }, :storage => :s3, :preserve_files => "true", :s3_credentials => "#{::Rails.root.to_s}/config/s3.yml", :path => "/:style/:filename",  :url  => ":s3_eu_url"

   attr_accessible :company, :email, :first_name, :isenabled, :last_name, :location, :password_digest, :profile_image, :role_id, :url_facebook, :url_twitter, :url_web, :username, :password, :password_confirmation, :location_id, :photo, :house_number, :address, :address_2, :city, :postcode, :country, :password_reset_token, :email_confirm_token, :auth_token

   has_secure_password
   validates_uniqueness_of :email
   validates :email, :email_format => {:message => 'invalid'}
   validates_uniqueness_of :username
   validates :username, format: { with: /\A[a-zA-Z0-9]+\Z/ }

   validates_presence_of :password, :on => :create
   validates_presence_of :username
   validates_presence_of :first_name
   validates_presence_of :last_name


   def the_transaction_status(dealid)
      if troopcompletes.where("deal_id = ?", dealid).first.transaction_status == 1
         return 'Completed'
      else
         return 'Incomplete'
      end
   end

   def gettrooplinkid(dealid)
      return troopcompletes.where("deal_id = ?", dealid).first
   end

   def has_like? product
      likes.find_by_product_id product.id
   end

   def has_trooplink? deal
      trooplinks.find_by_deal_id deal.id
   end


   def send_password_reset
      generate_token(:password_reset_token)
      self.password_reset_sent_at = Time.zone.now
      save!
      UserMailer.password_reset(self).deliver
   end


   def send_email_confirm
      generate_token(:email_confirm_token)
      self.email_confirm_sent_at = Time.zone.now
      save!
      UserMailer.email_confirm(self).deliver
   end

   def send_responder_mail_1
      UserMailer.responder_mail_1(self).deliver
   end

   def send_responder_mail_2
      UserMailer.responder_mail_2(self).deliver
   end

   def send_responder_mail_3
      UserMailer.responder_mail_3(self).deliver
   end

   def send_responder_mail_4
      UserMailer.responder_mail_4(self).deliver
   end

   def send_responder_mail_5
      UserMailer.responder_mail_5(self).deliver
   end

   def send_responder_mail_6
      UserMailer.responder_mail_6(self).deliver
   end


   def send_account_authorised
      UserMailer.account_authorised(self).deliver
   end


   def send_troop_joined(order,deal)
      UserMailer.troop_joined(self,order,deal).deliver
   end


   def send_troop_ended(trooplink)
      UserMailer.troop_ended(self, trooplink).deliver
   end

   def send_troop_failed(trooplink)
      UserMailer.troop_failed(self, trooplink).deliver
   end

   def new_deal_email(deal)
      UserMailer.new_deal_email(self, deal).deliver
   end

   def generate_token(column)
      begin
         self[column] = SecureRandom.urlsafe_base64
      end while User.exists?(column => self[column])
   end

   def self.search(search)
      if search
         where('first_name ilike ?', "%#{search}%")
      else
         scoped
      end
   end

   def self.from_omniauth(auth)
      where(auth.slice(:uid)).first_or_initialize.tap do |user|
         user.provider = auth.provider
         user.uid = auth.uid
         user.username = (auth.info.first_name+auth.info.last_name)
         user.first_name = auth.info.first_name
         user.last_name = auth.info.last_name
         user.email = auth.info.email
         user.password_digest = 'topsecretpasswordthatwillneverbefoundout'
         user.password = 'topsecretpasswordthatwillneverbefoundout'
         user.role_id = 3
         user.save!
      end
   end
   
end
