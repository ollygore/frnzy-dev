class Page < ActiveRecord::Base
   attr_accessible :content, :metadescription, :metakeyword, :metatitle, :slug, :title

   extend FriendlyId
   friendly_id :slug

   validates_presence_of :slug
   validates_uniqueness_of :slug

end

