class Location < ActiveRecord::Base

   extend FriendlyId
   friendly_id :slug

   has_many :deals
   has_many :users

   validates_presence_of :region
   validates_presence_of :description
   validates_presence_of :slug
   validates_uniqueness_of :slug


   accepts_nested_attributes_for :deals
   accepts_nested_attributes_for :users
   attr_accessible :description, :region, :slug, :gps_lat, :gps_long, :radius

end
