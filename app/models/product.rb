class Product < ActiveRecord::Base

   extend FriendlyId
   friendly_id :generate_custom_slug, use: :slugged

    def generate_custom_slug
      "#{name}"
    end 


   before_save :escape_filename

   belongs_to :category
   belongs_to :group
   belongs_to :user
   has_many :likes, :dependent => :destroy
   has_many :deals, :dependent => :destroy

   has_attached_file :product_image, :styles => { :extrasmall => "30x30#", :follow => "217x140#", :small => "228x173#", :square => "200x200#", :medium => "478x362#", :largebox => "604x458#", :original => "100%" }, :processors => [:cropper], :storage => :s3, :s3_credentials => "#{::Rails.root.to_s}/config/s3.yml", :preserve_files => true, :s3_permissions => :public_read, :path => "/:style/:filename", :url  => ":s3_eu_url"

   accepts_nested_attributes_for :deals
   accepts_nested_attributes_for :likes

   attr_accessible :barcode, :category_id, :description, :product_url, :image_url, :isenabled, :name, :price, :slug, :user_id, :product_image, :auction_end, :popularity, :group_id, :featured

   validates_presence_of :name
   # validates_presence_of :price
   validates_presence_of :category_id
   validates_presence_of :group_id
   validates_uniqueness_of :name
   
   acts_as_commentable

   def escape_filename
      require 'htmlentities'
      coder = HTMLEntities.new
      self.name = coder.decode(self.name)
   end

   def howmanylikes
      return self.likes.count
   end

   def self.search(search)
      if search
         self.fuzzy(search)
         #where('name ilike ?', "%#{search}%")
         #where('name LIKE ?', "%#{search}%")
      else
         scoped
      end
   end

   def self.fuzzy(search)
      if search
         keys = search.split(' ')

         ilike = [keys.map {|i| "name ilike ? "}.join(" AND ")]
         keys.each do |key|
            ilike.push( "%#{key}%" )
         end
         retval = where(ilike)
         Rails.logger.debug("My object: #{@some_object.inspect}")
         if where(ilike).count != 0 then
            return retval
         end
         ilike[0] = ilike[0].sub(/AND/,'OR')
         where(ilike)
      else
         scoped
      end
   end


   def self.searchwithcat(search,cat)

      catob = Category.where('slug = ?', cat)

      if search

         find(:all, :conditions => ['name ilike ? AND category_id = ?', "%#{search}%", catob])

      else
         find(:all)
      end
   end


end
