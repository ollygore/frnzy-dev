class Order < ActiveRecord::Base

   attr_accessible :total, :paypal_email, :paypal_name, :user_id, :name, :deal_id, :order_type, :transaction_paypal, :transactionid

   belongs_to :user
   belongs_to :deal

   has_many :trooplinks
   has_many :troopfails
   has_many :troopcompletes

   def self.search(search)
      if search
         where('id = ?', "#{search}")
      else
         scoped
      end
   end

end
