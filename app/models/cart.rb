class Cart < ActiveRecord::Base

   acts_as_shopping_cart_using :shopping_cart_item

   def shipping_cost
      0
   end

   def tax_pct
      0
   end

   def discount
      0
   end

   def total
      subtotal + taxes + shipping_cost
   end

end
