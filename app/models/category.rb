class Category < ActiveRecord::Base

   extend FriendlyId
   friendly_id :slug

   has_many :products
   has_many :groups, :dependent => :destroy

   validates_presence_of :name
   validates_presence_of :description
   validates_presence_of :slug
   validates_uniqueness_of :slug

   accepts_nested_attributes_for :products
   attr_accessible :description, :name, :slug

end
