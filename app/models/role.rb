class Role < ActiveRecord::Base
   has_many :users
   accepts_nested_attributes_for :users
   attr_accessible :role_type
end
