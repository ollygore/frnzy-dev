class Like < ActiveRecord::Base


   default_scope order('created_at ASC')



   attr_accessible :product_id, :user_id

   belongs_to :user
   belongs_to :product

   validates_uniqueness_of :product_id, :scope => :user_id

end
