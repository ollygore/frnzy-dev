class Trooplink < ActiveRecord::Base

   belongs_to :user
   belongs_to :deal
   belongs_to :order

   attr_accessible :deal_id, :user_id, :order_id

end
