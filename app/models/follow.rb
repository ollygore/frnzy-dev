class Follow < ActiveRecord::Base
  attr_accessible :follower_id, :user_id
  belongs_to :user

	validates_uniqueness_of :follower_id, :scope => :user_id


end
