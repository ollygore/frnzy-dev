class Deal < ActiveRecord::Base

   extend FriendlyId
   friendly_id :slug

   belongs_to :user
   belongs_to :product
   belongs_to :location
   has_many :trooplinks, :dependent => :destroy
   has_many :troopcompletes, :dependent => :destroy
   has_many :troopfails, :dependent => :destroy

   has_many :orders, :dependent => :destroy

   accepts_nested_attributes_for :trooplinks
   accepts_nested_attributes_for :troopcompletes
   accepts_nested_attributes_for :troopfails

   attr_accessible :auction_end, :isenabled, :product_id, :slug, :status, :troop_limit, :price, :user_id, :location_id, :published_at_date, :published_at_time, :end_count, :popularity, :featured

   attr_accessible :troop_rrp
   validates_presence_of :price
   validates_presence_of :troop_limit
   validates_presence_of :troop_rrp, :message => "^RRP can't be blank"
   validates_presence_of :auction_end

   validates_presence_of :product_id
   validates_presence_of :user_id
   validates_uniqueness_of :slug

   acts_as_commentable

   #after_create :send_want_emails

   def send_want_emails
      if self.product.likes.count > 0 
         self.product.likes.each do |like|
            like.user.delay.new_deal_email(self)
         end
      end
   end

   def percentsaving
       saving = ((1-(self.price/self.troop_rrp))*100).round()
       return saving.to_i
   end

   def the_transaction_status
      if troopcompletes.first.transaction_status == 1
         return 'Completed'
      else
         return 'Incomplete'
      end
   end

   def self.search(search)
      if search
         joins(:product).where('name ilike ?', "%#{search}%")
      else
         find(:all)
      end
   end

   def self.searchwithcat(search,cat)
      catob = Category.where('slug = ?', cat)
      if search
         joins(:product).where('name ilike ? AND category_id = ?', "%#{search}%", catob)
      else
         find(:all)
      end
   end

end
