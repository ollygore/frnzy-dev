module PaypalExpressHelper
   def get_setup_purchase_params(cart, request)

      session[:orderlink] = SecureRandom.hex(5)

      subtotal, shipping, total, tax = get_totals(cart)
      return to_cents(total), {
         :ip => request.remote_ip,
         :return_url => url_for(:action => 'review', :only_path => false),
         :cancel_return_url => url_for(:action => 'cancel', :only_path => false),
         :subtotal => to_cents(subtotal),
         :shipping => to_cents(shipping),
         :currency => 'GBP',
         :handling => 0,
         :tax => to_cents(tax),
         :allow_note => true,
         :items => get_items(cart),
      }


   end


   def get_purchase_params(cart, request, params)
      subtotal, shipping, total, tax = get_totals(cart)
      return to_cents(total), {
         :ip => request.remote_ip,
         :token => params[:token],
         :payer_id => params[:payer_id],
         :subtotal => to_cents(subtotal),
         :shipping => to_cents(shipping),
         :currency => 'GBP',
         :handling => 0,
         :tax => to_cents(tax),
         :items => get_items(cart),
      }
   end


   def get_order_info(gateway_response, cart)
      subtotal, shipping, total, tax = get_totals(cart)
      {
         shipping_address: gateway_response.address,
         email: gateway_response.email,
         name: gateway_response.name,
         gateway_details: {
            :token => gateway_response.token,
            :payer_id => gateway_response.payer_id,
         },
         subtotal: subtotal,
         shipping: shipping,
         total: total,
      }
   end



   def get_shipping(cart)
      # define your own shipping rule based on your cart here
      # this method should return an integer
   end

   def get_items(cart)

      @cart.shopping_cart_items.collect do |line_item|
         deal = line_item.item

         {
            :name => deal.product.name,
            :number => session[:orderlink],
            :quantity => line_item.quantity,
            :amount => to_cents(deal.price),
         }
      end

   end

   def get_totals(cart)
      subtotal = @cart.subtotal
      shipping = @cart.shipping_cost
      total = @cart.total
      tax = @cart.taxes
      puts 'toger'
      puts subtotal, shipping, total
      return subtotal, shipping, total, tax
   end


   def to_cents(money)
      (money*100).round
   end
end
