#coding: utf-8
class UserMailer < ActionMailer::Base

   default from: '"FRNZY" <team@frnzy.co.uk>'


   def password_reset(user)
      @user = user
      mail :to => user.email, :subject => "FRNZY: Password reset request"
   end


   def email_confirm(user)
      @user = user
      mail :to => user.email, :subject => "FRNZY would like to confirm your email address"
   end


   def account_authorised(user)
      @user = user
      mail :to => user.email, :subject => "Your FRNZY account is ready"
   end


   def troop_joined(user,order,troop)
      @user = user
      @order = order
      @deal = troop
      mail :to => user.email, :subject => "Let the FRNZY begin!"
   end


   def troop_ended(user, trooplink)
      @user = user
      @deallink = trooplink
      mail :to => user.email, :subject => "FRNZY: It's a done deal!"
   end


   def troop_failed(user, trooplink)
      @user = user
      @deallink = trooplink
      mail :to => user.email, :subject => "FRNZY: We're sorry, the deal didn't tip!"
   end


   def new_contactmessage(contactmessage)
       @contactmessage = contactmessage
       mail(:subject => ( "Contact submission from " + @contactmessage.name ),:to => "admin@frnzy.co.uk", :bcc => "olly@goreblimey.com") do |format|
         format.html
      end
   end


   def invite_friend_to_product(user, emailAddrs, product, referral)
      @user = user
      @product = product
      @referral = referral
      mail(:subject => ( "Check out this product on FRNZY!" ),:to => emailAddrs) do |format|
         format.html
      end
   end


   def invite_friend_to_deal(user, emailAddrs, deal, referral)
      @user = user
      @deal = deal
      @referral = referral
      mail(:subject => ( "Check out this deal on FRNZY!" ),:to => emailAddrs) do |format|
         format.html
      end
   end

   def invite_friend_to_home(user, emailAddrs, referral)
      @user = user
      @referral = referral
      mail(:subject => ( "Join the FRNZY!" ),:to => emailAddrs) do |format|
         format.html
      end
   end


   def responder_mail_1(user)
      @user = user
      mail :to => user.email, :subject => "What is FRNZY?"
   end

   def responder_mail_2(user)
      @user = user
      mail :to => user.email, :subject => "Adding products on FRNZY is easy!"
   end

   def responder_mail_3(user)
      @user = user
      mail :to => user.email, :subject => "Deals on FRNZY"
   end

   def responder_mail_4(user)
      @user = user
      mail :to => user.email, :subject => "Invite your friends to FRNZY"
   end

   def responder_mail_5(user)
      @user = user
      mail :to => user.email, :subject => "Follow others on FRNZY"
   end

   def responder_mail_6(user)
      @user = user
      mail :to => user.email, :subject => "Win urBeats by Dre worth £80 on FRNZY" 
   end

   
   def new_deal_email(user, deal)
      @deal = deal
      @user = user
      mail :to => user.email, :bcc => 'olly@goreblimey.com, jezmurrell@gmail.com', :subject => "We've created a deal for you!"
   end



end
